# Resolving Conflicts in Role-based Multimodel-Synchronization

Supplementary material for the [COP 2021](https://conf.researchr.org/track/ecoop-issta-2021/ecoop-issta-2021-cop) publication *"Resolving Synchronization Conflicts in Role-based Multimodel-Synchronization Environments"* by Sebastian Ebert, Tim Kluge , Sebastian Götz, Uwe Aßmann and Christopher Werner.

The repository contains the source code of a library to be used as a core for systems synchronizing heterogenous object-oriented models. 

## Prerequisites

* Java SE Development Kit 8 (minimum)
* Maven 3.6.0 (minimum)

## Example

A minimal example project is available in `quickstart`, which just creates a new element at the central hub.

To run it, clone the repository and run `mvn package exec:java` in `quickstart`.

## Development

If you want to work on the actual library:
### Installation

This project is based on Maven, thus building it is simple:

1. clone this project into your workspace
2. cd into the cloned project
3. run mvn -compile
4. run mvn -package

### Setting up IntelliJ

1. Install IntelliJ's Plugin for Scala-Support
2. Clone this maven project
3. Import the project into IntelliJ
4. Wait till all dependencies are downloaded / installed

### Example Model Code

* see https://git-st.inf.tu-dresden.de/rosi/parallel-rsync/-/tree/master/crm-example
