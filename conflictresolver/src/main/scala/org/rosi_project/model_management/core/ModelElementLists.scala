package org.rosi_project.model_management.core

import org.slf4j.LoggerFactory

import org.rosi_project.model_management.sync.lock._

import java.util.concurrent.ConcurrentHashMap

/**
 * Provides convenient access to all instances of the synchronized models.
 *
 * New models may be registered by name to simplify retrieving their instances.
 *
 * Retrieval operations (including get) generally do not block, so may overlap with update operations (including put and remove).
 */
object ModelElementLists {

  // element in top-hash-map is only locked on model-creation
  // top-level: models, second-level: elements
  var elements: ConcurrentHashMap[Class[_ <: AnyRef], ConcurrentHashMap[String, AnyRef]] =
  new ConcurrentHashMap[Class[_ <: AnyRef], ConcurrentHashMap[String, AnyRef]]()

  var model2Class: ConcurrentHashMap[String, Class[_ <: AnyRef]] = new ConcurrentHashMap[String, Class[_ <: AnyRef]]()

  val logger = LoggerFactory.getLogger(ModelElementLists.getClass)

  def getElementByGuid(guid: String): AnyRef = {

    elements.keySet().forEach { e =>
      val elem: AnyRef = elements.get(e).get(guid)
      if (elem != null) {
        return elem
      }
    }
    null
  }

  /**
   * Inserts a new instance.
   *
   * The appropriate model will be inferred automatically
   *
   * @param obj the instance. May never be `null`
   */
  def addElement(obj: Object): Unit = {

    if (obj == null) {
      return
    }

    var elementsWithClass = elements.get(obj.getClass)
    if (elementsWithClass == null) {
      logger.info("" + elements + " / " + obj)
      initThreadSave(obj, elementsWithClass)
      //elements.put(obj.getClass, new ConcurrentHashMap[String, AnyRef]())
      elementsWithClass = elements.get(obj.getClass)
    }

    if (elementsWithClass.isEmpty) {
      var value: ConcurrentHashMap[String, AnyRef] = new ConcurrentHashMap[String, AnyRef]()
      value.put(obj.asInstanceOf[SynchronizationAware].guid, obj)
      elements.put(obj.getClass, value)
    } else {
      elementsWithClass.put(obj.asInstanceOf[SynchronizationAware].guid, obj)
    }
  }

  /**
   * Makes an Threadsave init of the elementsWithClass-map. Reason: If to threads are creating a the first element of the same
   * model at the same time, one element would be overridden.
   */
  private def initThreadSave(obj: Object, elementsWithClass: ConcurrentHashMap[String, AnyRef]): Unit = {

    ElementLockProvider.synchronized {
      logger.info("### save init ###")
      if (elementsWithClass == null) {
        elements.put(obj.getClass, new ConcurrentHashMap[String, AnyRef]())
      }
    }
  }

  /**
   * Removes an element from the ModelElementsLists.
   */
  def removeElement(obj: AnyRef): Unit = {

    if (obj == null) {
      return
    }

    var elementsWithClass: ConcurrentHashMap[String, AnyRef] = elements.get(obj.getClass)
    elementsWithClass.remove(obj.asInstanceOf[SynchronizationAware].guid)
  }

  /**
   * Informs the repository about what objects belong to which model
   *
   * @param name      the model's name
   * @param elemsType the class of the corresponding instances
   * @throws IllegalArgumentException if the name is already in use
   */
  def registerModel(name: String, elemsType: Class[_ <: AnyRef]): Unit = {
    if (model2Class.contains(name)) {
      throw new IllegalArgumentException(s"Model is already present: $name")
    }

    model2Class.put(name, elemsType)
  }

  /**
   * Returns elements of which classname contains the parameter.
   *
   * @return the elements
   */
  def getElementsFromType(s: String): Set[AnyRef] = {

    var elementSet: Set[AnyRef] = Set()

    elements.keySet().forEach { k =>
      if (k.getName.contains(s)) {
        elements.get(k).values().forEach { element =>
          elementSet += element
        }
      }
    }

    return elementSet
  }

  /**
   * Returns elements by their classname.
   *
   * @param s the classname
   * @return the elements
   */
  def getDirectElementsFromType(s: String): Set[SynchronizationAware] = {
    var elementSet: Set[SynchronizationAware] = Set()

    elements.keySet().forEach { k =>
      if (k.getName == s) {
        elements.get(k).values().forEach { element =>
          elementSet += element.asInstanceOf[SynchronizationAware]
        }
      }
    }

    return elementSet
  }

  /**
   * Resets ModelElementsLists by cleaning all maps.
   */
  def reset() {
    elements.keySet().forEach { modelKey =>
      elements.get(modelKey).values().forEach { element =>
        element.asInstanceOf[PlayerSync].deleteObjectFromSynchro()
      }
    }

    dropAll()
  }

  def dropAll(): Unit ={
    this.elements = new ConcurrentHashMap[Class[_ <: AnyRef], ConcurrentHashMap[String, AnyRef]]()
    this.model2Class = new ConcurrentHashMap[String, Class[_ <: AnyRef]]()
  }

  /**
   * Print out all elements to console.
   */
  def printAll(): Unit = {

    elements.keySet().forEach { modelKey =>
      println("Model: " + modelKey)

      elements.get(modelKey).values().forEach { element =>
        println("** " + element)
      }
    }
  }

  /**
   * Log out alle elements on info-level.
   */
  def logAll(): Unit = {
    elements.keySet().forEach { modelKey =>
      logger.info("Model: " + modelKey)

      elements.get(modelKey).values().forEach { element =>
        logger.info("** " + element)
      }
    }
  }

  /**
   * Log out alle elements on error-level.
   */
  def logAllOnError(): Unit = {
    elements.keySet().forEach { modelKey =>
      logger.error("Model: " + modelKey)

      elements.get(modelKey).values().forEach { element =>
        logger.error("** " + element)
      }
    }
  }
}
