package org.rosi_project.model_management.core

object Cardinality extends Enumeration {
  type Cardinality = Value
  val One, Many = Value
}
