package org.rosi_project.model_management.core

import org.rosi_project.model_management.sync.lock.{ElementLock, LockResult, SynchronizationAware}

import scala.collection.parallel
import scala.collection.parallel.mutable.ParMap
import scala.reflect.{ClassTag, classTag}
import scala.sys.error

case class ModelInformation(c: Class[_], parentClass: Class[_], fields: Seq[Field] = Seq())

object ModelInformation {
  private val models: parallel.mutable.ParMap[Class[_], Set[Field]] = ParMap()
  private val modelsParentClass: parallel.mutable.ParMap[Class[_], Class[_]] = ParMap().withDefaultValue(null)

  def add(m: ModelInformation*): Unit = {
    synchronized {
      m.foreach { x =>
        models.put(x.c, x.fields.toSet)
        if (x.parentClass != null) modelsParentClass.put(x.c, x.parentClass)
      }
    }
  }

  /**
   * Returns the type of a association field in a registered class
   * @param typ Full name of class
   * @param field Name of field
   */
  def getAssociationFieldType(typ: String, field: String): String =
    models(Class.forName(typ))
    .find(f => f.name == field).getOrElse(error(s"Field ${field} not found for type ${typ}")).typ.runtimeClass.getName

  def fieldsByName(c: String): Set[Field] = {
    val cl = Class.forName(c).asSubclass(classTag[ConflictResolvable].runtimeClass)
    val parent = modelsParentClass(cl)
    val parentFields = if(parent != null) fieldsByName(c) else Set()

    models(cl) ++ parentFields
  }

  def modelExists(c: String): Boolean = {
      models.contains(Class.forName(c))
  }

  def getGuid(c: SynchronizationAware): String = if(c != null) c.guid else ""

  def getGuids(c: Set[SynchronizationAware]): String = c.filter(x => x != null).map(x => x.guid).mkString("|")

  def fromGuid(c: String): SynchronizationAware = if(c != "") ModelElementLists.getElementByGuid(c).asInstanceOf[SynchronizationAware] else null.asInstanceOf[SynchronizationAware]

  def fromGuids(t: ClassTag[_], c: String): Set[SynchronizationAware] = {
    val list = ModelElementLists.getDirectElementsFromType(t.runtimeClass.getName).map(x => (x.guid, x)).toMap
    c.split('|').map(x => if(x != ""){
      list(x)
    } else null).toSet[SynchronizationAware].filter(x => x != null)
  }

}

 trait MapSerializable {
  def serialize: Map[String, String] = Map()

  def unserialize(data: Map[String, String]) = {}
 }
