package org.rosi_project.model_management.core

import scroll.internal.MultiCompartment

import org.rosi_project.model_management.sync.lock._

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Main interface for all elements. Integrates an element to RSYNC.
 */
trait PlayerSync extends MultiCompartment {

  val logger = LoggerFactory.getLogger(classOf[PlayerSync])

  buildClass()

  var deleted: Boolean = false
  var ts: Int = 0
  var rv: Int = 0

  def isDeleted: Boolean = deleted

  /**
   * Builds and integrates the implementing element to RSYNC.
   */
  def buildClass(): Unit = {
    ElementLockProvider.synchronized {
      if (!SynchronizationCompartment.isUnderConstruction()) {
        SynchronizationCompartment combine this
        val mani = SynchronizationCompartment.createRoleManager()
        this play mani
        mani.manage(this)
      }
    }
  }

  /**
   * Removes the implementing element from RSYNC.
   */
  def deleteObjectFromSynchro(): Unit = {
    ElementLockProvider.synchronized {
      +this deleteManage this
      deleted = true
    }
  }

  /**
   * Returns relating elements in other models.
   */
  def getRelatedObjects(): Set[PlayerSync] = {
    ElementLockProvider.synchronized {
      var relatedElementsMetaObject = +this getRelatedElements ()
      logger.error("rel:" + relatedElementsMetaObject);
      try{
    	  var relatedObjects: Set[PlayerSync] = relatedElementsMetaObject.right.get.head.right.get
    			  return relatedObjects
      } catch{
        case e : NoSuchElementException => logger.error("this should not occur...")
        return Set()
      }
    }
  }
}
