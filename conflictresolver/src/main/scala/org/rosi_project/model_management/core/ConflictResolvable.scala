package org.rosi_project.model_management.core

import org.rosi_project.model_management.sync.lock.{ElementLock, SynchronizationAware}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.reflect.classTag
import scala.sys.error

abstract class ConflictResolvable(lock: ElementLock, guid: String, owner: String) extends SynchronizationAware(lock, guid, owner) with MapSerializable {
  def syncFieldsToOtherModels()

  override def equals(other: Any): Boolean = {
    other match {
      case p: ConflictResolvable => guid.equals(p.guid)
      case _ => false
    }
  }

  override def hashCode(): Int = guid.hashCode

  override def serialize(): Map[String, String] = {
    Map("guid" -> guid, "owner" -> owner)
  }

  def getAssociations: Set[AssociationField] = ModelInformation.fieldsByName(getClass.getName).filter(p => p.isInstanceOf[AssociationField]).map(p => p.asInstanceOf[AssociationField])

  def getAssociated: mutable.Map[AssociationField, mutable.Set[ConflictResolvable]] = {
    val serialized = serialize()

    val result = mutable.Map[AssociationField, mutable.Set[ConflictResolvable]]()
    val associations = getAssociations

    associations.foreach { k =>
      result.put(k, mutable.Set[ConflictResolvable](ModelInformation.fromGuids(k.typ, serialized(k.name))
        .map(x => x.asInstanceOf[ConflictResolvable]).toSeq:_*))
    }

    result
  }

  def getReferenced(associations: Set[String]): Set[ConflictResolvable] = {
    val data = serialize()

    associations.flatMap { f => ModelInformation.fromGuids(classTag[ConflictResolvable], data(f)) }.map(x => x.asInstanceOf[ConflictResolvable])
  }

  def removeAssociation(element: ConflictResolvable): Unit = {
    val associated = getAssociated

    associated.foreach { case (field, elements) =>
      val found = elements.contains(element)
      if(found){
        val f: AssociationField = ModelInformation.fieldsByName(element.getClass.getName).find(p => p.name == field).get.asInstanceOf[AssociationField]
        f.onRemove(this, element)
      }
    }

    error("Not found")
  }
}
