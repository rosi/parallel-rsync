package org.rosi_project.model_management.core

import org.rosi_project.model_management.core.Cardinality.Cardinality

import scala.reflect.ClassTag

abstract class Field {
  def name: String
  def typ: ClassTag[_]
}

object Field {
  def primitives(typ: ClassTag[_], names: String*): Seq[Field] = names.map(x => PrimitiveField(x, typ))
  def association(typ: ClassTag[_], name: String, cardinality: Cardinality, onAdd: (ConflictResolvable, ConflictResolvable) => Unit, onRemove: (ConflictResolvable, ConflictResolvable) => Unit): Field = AssociationField(name, typ, cardinality, onAdd, onRemove)
}

case class PrimitiveField(name: String, typ: ClassTag[_]) extends Field()
case class AssociationField(name: String, typ: ClassTag[_], cardinality: Cardinality, onAdd: (ConflictResolvable, ConflictResolvable) => Unit, onRemove: (ConflictResolvable, ConflictResolvable) => Unit) extends Field()
