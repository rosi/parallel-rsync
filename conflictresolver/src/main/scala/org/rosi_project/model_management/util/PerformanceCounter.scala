package org.rosi_project.model_management.util

import java.io.{File, PrintWriter}
import java.lang.ThreadLocal
import scala.collection.mutable.ListBuffer

case class PerformanceInformation(thread: Long, lockFailures: Int, name: String, time: Long, numElements: Int, threads: Int)

case class RsyncOnlyPerformanceInformation(name: String, time: Long, numElements: Int)

object PerformanceCounter {
  def writeRecords(iterations: Seq[PerformanceInformation], path: String): Unit = {
    synchronized {
      val file = new File(path)
      val printWriter = new PrintWriter(file)
      printWriter.write("thread,lockFailures,name,time,numElements,threads\n")
      iterations.foreach(p => {
        printWriter.write(s"${p.thread},${p.lockFailures},${p.name},${p.time},${p.numElements},${p.threads}\n")
      })
      printWriter.close()
    }
  }

  def writeRsyncRecords(iterations: Seq[RsyncOnlyPerformanceInformation], path: String): Unit = {
    synchronized {
      val file = new File(path)
      val printWriter = new PrintWriter(file)
      printWriter.write("name,time,numElements\n")
      iterations.foreach(p => {
        printWriter.write(s"${p.name},${p.time},${p.numElements}\n")
      })
      printWriter.close()
    }
  }
}
