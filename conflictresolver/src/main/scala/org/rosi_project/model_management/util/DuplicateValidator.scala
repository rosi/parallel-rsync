package org.rosi_project.model_management.util

object DuplicateValidator {
  /**
   * Returns true if the given sequence contains any duplicate elements
   */
  def containsDuplicates[T](a: Seq[T]): Boolean = a.toSet.size < a.size
}
