package org.rosi_project.model_management.util

import java.util.UUID.randomUUID

/**
 * Class that encapsulates the UUID-generation used for GUID's.
 */
object UuidUtil {
  
  def generateUuid(): String = {
	    return randomUUID().toString
  }
}