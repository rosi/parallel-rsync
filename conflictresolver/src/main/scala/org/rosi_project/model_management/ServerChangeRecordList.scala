package org.rosi_project.model_management

/* object ChangeRecordMode extends Enumeration {
  type ChangeRecordMode = Value
  val Creation, Deletion, Modification = Value
} */

case class ClientChangeRecord(val model: String, val guid: String, val rv: Int, val ts: Int,
                   val updatedFields: Map[String, String],
                   val addedReferences: Map[String, Set[String]],
                   val deletedReferences: Map[String, Set[String]],
                   val lokalId: String,
                   val owner: String,
                   val refonly: Boolean) {

  /**
   * Returns an iterable of tuples (fieldname, referenced guid)
   */
  def addedReferencesTuples: Iterable[(String, String)] = addedReferences.map { case (field, ids) => ids.map(id => (field, id)) }.flatten

  /**
   * Returns an iterable of tuples (fieldname, referenced guid)
   */
  def deletedReferencesTuples: Iterable[(String, String)] = deletedReferences.map { case (field, ids) => ids.map(id => (field, id)) }.flatten

  /**
   * Returns an iterable tuples (fieldname, reference guid)
   */
  def changedReferences: Iterable[(String, String)] = (addedReferencesTuples ++ deletedReferencesTuples)
}

case class ServerChangeRecord(model: String, guid: String, ts: Int, rv: Int, refonly: Boolean, lokalId: String, owner: String,
                              fields: Map[String, String])

case class ServerReferenceChangeRecord(model: String, guid: String, ts: Int, rv: Int, refs: Seq[String])