package org.rosi_project.model_management.sync.conflict.customrules

import org.rosi_project.model_management.core.PlayerSync

/**
 * Interface for all custom deletion-rules.
 */
trait ICustomDeletionRule {

  /**
   * get name of this rule
   */
  def getRuleName(): String

  /**
   * true if element matches the rule
   */
  def matchesRule(element: PlayerSync, senderOwner: String): Boolean
}