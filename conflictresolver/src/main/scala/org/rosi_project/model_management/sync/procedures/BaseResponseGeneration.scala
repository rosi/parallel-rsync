package org.rosi_project.model_management.sync.procedures

import org.rosi_project.model_management.{ServerChangeRecord, ServerReferenceChangeRecord}
import org.rosi_project.model_management.sync.procedures.data._
import org.rosi_project.model_management.sync.snapshot._
import org.rosi_project.model_management.sync.lock._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
 * Base class for all synchronization-algorithms (phase of generating the SCR).
 */
trait BaseResponseGeneration {

  /**
   * Builds the SCR. Input date comes from the corresponding synchronization-service.
   * 	cer -> from request
   * 	idMapping -> maps global to local IDs for new elements
   * 	notUpdatedOnServer -> IDs of elements where update was not applies & RV/TS is not higher
   */
  def buildResponse(models: Set[String], idMapping: Map[String, String], cer: Seq[ClientEntireRecord], notUpdatedOnServer: Set[String], owner: String): mutable.Map[String, ListBuffer[ServerChangeRecord]]

  /**
   * Build the data-part of the SCR.
   * 	snapshot -> has to be created in buildResponse (list of meta-informations about element at response-generation-time)
   * 	idMapping -> maps global to local IDs for new elements
   * 	notUpdatedOnServer -> IDs of elements where update was not applies & RV/TS is not higher
   *  records -> will be filled with datachanges
   */
  def buildDataChanges(snapshot: ListBuffer[Snapshot], idMapping: Map[String, String], cer: Seq[ClientEntireRecord], notUpdatedOnServer: Set[String],
                       lock: ElementLock, records: mutable.Map[String, ListBuffer[ServerChangeRecord]], owner: String): Int

  /**
   * Build the data-part of the SCR.
   * 	snapshot -> has to be created in buildResponse (list of meta-informations about element at response-generation-time)
   * 	idMapping -> maps global to local IDs for new elements
   * 	notUpdatedOnServer -> IDs of elements where update was not applies & RV/TS is not higher
   *  records -> filled with datachanges by buildDataChanges-method
   *  referenerecords -> will be filled with referencechanges
   *  processedReferencingElements -> already for reference-part of response processed elements with reference-changes
   */
  def buildReferenceChanges(snapshot: ListBuffer[Snapshot], idMapping: Map[String, String], cer: Seq[ClientEntireRecord], notUpdatedOnServer: Set[String], lock: ElementLock,
                            records: mutable.Map[String, ListBuffer[ServerChangeRecord]], referenceRecords: ListBuffer[ServerReferenceChangeRecord],
                            processedReferencingElements: ListBuffer[String], owner: String): Int
}