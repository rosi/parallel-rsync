package org.rosi_project.model_management.sync.conflict.systemrules

import org.rosi_project.model_management.core.PlayerSync

/**
 * Interface for all system-rules for data-changes (modification and deletions).
 */
trait DataConflictSystemRule {
  
  /**
   * returns the rule name
   */
  def getRuleName(): String
  
  /**
   * returns true + target-element, if rule detects no modification-conflict
   */
  def resolveModificationConflicts(ts: Int, modifier: String, guid: String, elementKey: String): (Boolean, PlayerSync)
  
  /**
   * returns true + target-element, if rule detects no deletion-conflict
   */
  def resolveDeletionConflicts(ts: Int, modifier: String, guid: String, elementKey: String): (Boolean, PlayerSync)
  
}