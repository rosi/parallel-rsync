package org.rosi_project.model_management.sync.conflict.systemrules

import scala.collection.mutable.ListBuffer

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.rosi_project.model_management.core.PlayerSync

case class DeletionDataConflictResolutionResult(wasDeleted: Boolean, sync: PlayerSync)

/**
 * Allows the execution of a row system rules combined by logical OR.
 */
object SystemRuleExecutor {
  
  private val logger = LoggerFactory.getLogger(SystemRuleExecutor.getClass)
  private var refRules: ListBuffer[ReferenceConflictSystemRule] = new ListBuffer[ReferenceConflictSystemRule]()
  private var dataRules: ListBuffer[DataConflictSystemRule] = new ListBuffer[DataConflictSystemRule]()
  
  /**
   * returns all system reference rules
   */
  def getRefRules(): ListBuffer[ReferenceConflictSystemRule] = {
    return refRules
  }
  
  /**
   * returns all system data rules
   */
  def getDataRules(): ListBuffer[DataConflictSystemRule] = {
    return dataRules
  }
  
  /**
   * logs out all rules that the SystemRuleExecutor knows
   */
  def logRules(): Unit = {
    logger.debug("*** System Reference Rules:")
    
    refRules.foreach{ r =>
      logger.debug("****** " + r.getRuleName())
    }
    
    logger.debug("*** System Data Rules:")
    
    dataRules.foreach{ r =>
      logger.debug("****** " + r.getRuleName())
    }
  }

  /**
   * adds a system data rule to the SystemRuleExecutor
   */
  def addSystemDataRule(rules: DataConflictSystemRule*): Unit = {
    rules.foreach { rule =>
      dataRules.foreach { r =>
        if (r.getRuleName() == rule.getRuleName()) {
          return
        }
      }
      dataRules += rule
    }
  }

  /**
   * removes a system data rule from the SystemRuleExecutor
   */
  def removeSystemDataRule(ruleName: String): Unit = {
    dataRules.foreach { r =>
      if (r.getRuleName() == ruleName) {
        dataRules -= r
        return
      }
    }
  }

  /**
   * adds a system reference rule to the SystemRuleExecutor
   */
  def addSystemReferenceRule(rule: ReferenceConflictSystemRule): Unit = {
    refRules.foreach { r =>
      if (r.getRuleName() == rule.getRuleName()) {
        return
      }
    }
    refRules += rule
  }

  /**
   * removes a system data rule from the SystemRuleExecutor
   */
  def removeSystemReferenceRule(ruleName: String): Unit = {
    refRules.foreach { r =>
      if (r.getRuleName() == ruleName) {
        refRules -= r
        return
      }
    }
  }

  /**
   * resolve data-modification-conflicts and returns true + target-element, if a minimum 1 rule detects no conflict
   */
  def resolveDataModificationConflicts(ts: Int, modifier: String, guid: String, elementKey: String): (Boolean, PlayerSync) = {
    dataRules.foreach{ r =>
      var res: (Boolean, PlayerSync) = r.resolveModificationConflicts(ts, modifier, guid, elementKey)
      
      // logical OR
      if(res._1){
        return res
      }
    }
    return (false, null)
  }
  
   /**
   * resolve data-deletion-conflicts and returns true + target-element, if a minimum 1 rule detects no conflict
   */
  def resolveDataDeletionConflicts(ts: Int, modifier: String, guid: String, elementKey: String): (Boolean, PlayerSync) = {
    dataRules.foreach{ r =>
      var res: (Boolean, PlayerSync) = r.resolveDeletionConflicts(ts, modifier, guid, elementKey)
      
      // logical OR
      if(res._1){
        return res
      }
    }
    return (false, null)
  }
  
   /**
   * resolve deleted-referenced-element-conflicts and returns true + target-element, if a minimum 1 rule detects no conflict
   */
  def resolveDeletedReferencedElementReferenceConflicts(guid: String, elementKey: String): PlayerSync = {
    refRules.foreach{ r =>
      var res: PlayerSync = r.resolveDeletedReferencedElementConflicts(guid, elementKey)
      
      // logical OR
      if(res != null){
        return res
      }
    }
    return null
  }
  
  /**
   * resolve deleted-referencing-element-conflicts and returns true + target-element, if a minimum 1 rule detects no conflict
   */
  def resolveDeletedReferencingElementReferenceConflicts(guid: String, elementKey: String): PlayerSync = {
    refRules.foreach{ r =>
      var res: PlayerSync = r.resolveDeletedReferencingElementConflicts(guid, elementKey)
      
      // logical OR
      if(res != null){
        return res
      }
    }
    return null
  }
}