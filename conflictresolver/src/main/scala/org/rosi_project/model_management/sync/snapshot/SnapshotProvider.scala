package org.rosi_project.model_management.sync.snapshot

import scala.collection.mutable.ListBuffer

import org.rosi_project.model_management.core.ModelElementLists
import org.rosi_project.model_management.core.PlayerSync
import org.rosi_project.model_management.sync.lock._

/**
 * Singleton object for creating snapshots. May used for response generation.
 */
object SnapshotProvider {
  
  /**
   * Creates a snapshot-list for elements (definition of snapshot-element see snapshot.scala).
   * 	modelElementKeys -> keys of element-types of elements which will be included in snapshot
   */
  def provideSnapshot(modelElementKeys: Set[String]) : ListBuffer[Snapshot] = {
    val snapshots : ListBuffer[Snapshot] = new ListBuffer[Snapshot]()
    
    modelElementKeys.foreach{ mek =>
      val elements: Set[SynchronizationAware] = ModelElementLists.getDirectElementsFromType(mek)
      
      elements.foreach{ e =>
        val player: PlayerSync = e.asInstanceOf[PlayerSync]
        snapshots += Snapshot(player.asInstanceOf[SynchronizationAware].guid, player.ts, player.rv, mek)
      }
    }

    snapshots
  }
  
  /**
   * Creates a deep copy of a snapshot-list.
   */
  def copySnapshot(original: ListBuffer[Snapshot]) : ListBuffer[Snapshot] = {
    
    var copy: ListBuffer[Snapshot] = new ListBuffer[Snapshot]()
    
    original.foreach{ o =>
      copy += new Snapshot(o.guid, o.ts, o.rv, o.elementKey)
    }
    return copy
  }
  
  /**
   * Checks if a snapshot-list contains an element with GUID.
   */
  def snapshotContains(snapshot: ListBuffer[Snapshot], guid: String): Boolean = {
    
    snapshot.foreach{ s =>
      if(s.guid == guid){
        return true
      }
    }
    return false
  }
}