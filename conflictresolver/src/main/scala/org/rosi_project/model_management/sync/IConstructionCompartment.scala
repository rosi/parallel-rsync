package org.rosi_project.model_management.sync

import scroll.internal.Compartment

import org.rosi_project.model_management.core._
import org.rosi_project.model_management.sync.helper.ConstructionContainer
import org.rosi_project.model_management.sync.roles.IConstructor

/**
 * PART OF RSYNC-CORE by Christopher Werner (TUD)
 * Interface for each construction rule.
 */
trait IConstructionCompartment extends Compartment {

  /**
   * Return a role instance that handles the construction process for the object.
   */
  def getConstructorForClassName(classname: Object): IConstructor

  def getRuleName: String

  private def addExtensionRoles(containers: Set[ConstructionContainer]): Unit = {
    containers.foreach { cc =>
      if (cc.isConstructed) {
        SynchronizationCompartment.getExtensions().foreach { e =>
          var role = e.getExtensionForClassName(cc.getPlayerInstance())
          if (role != null) {
            cc.getManagerInstance() play role
          }
        }
      }
    }
  }

  private def notifyExtensionRoles(containers: Set[ConstructionContainer]): Unit = {
    if (!SynchronizationCompartment.getExtensions().isEmpty) {
      containers.foreach { cc =>
        if (cc.isConstructed) {
          var playerInstance = cc.getPlayerInstance()
          +playerInstance insertNotification ()
        }
      }
    }
  }

  /**
   * Add the RoleManager roles from the synchronization compartment if necessary
   */
  protected def addManagerRoles(containers: Set[ConstructionContainer]): Unit = {
    containers.foreach { cc =>
      if (cc.isConstructed && !cc.isStartElement) {
        cc.getPlayerInstance play cc.getManagerInstance
      }
    }
  }

  /**
   * Add the delete roles for the elements in the ConstructionContainers.
   */
  protected def addDeleteRoles(containers: Set[ConstructionContainer]): Unit = {
    containers.foreach { cc =>
      if (cc.isConstructed) {
        cc.getManagerInstance() play SynchronizationCompartment.getDestructionRule().getDestructorForClassName(cc.getPlayerInstance())
      }
    }
  }

  /**
   * Add the related RoleManagers for the elements in the ConstructionContainers.
   */
  protected def addRelatedRoleManager(containers: Set[ConstructionContainer]): Unit = {
    containers.foreach { cc =>
      containers.foreach { inner =>
        cc.getManagerInstance.addRelatedManager(inner.getManagerInstance)
      }
    }
  }

  /**
   * Combine the SynchronizationCompartment with all Players from the ConstructionContainers.
   */
  protected def synchronizeCompartments(containers: Set[ConstructionContainer]): Unit = {
    containers.foreach { cc =>
      if (cc.isConstructed() && !cc.isStartElement()) {
        SynchronizationCompartment combine cc.getPlayerInstance
      }
    }
  }

  /**
   * Create the Synchronization mechanisms for the elements in the ConstructionContainers.
   */
  protected def bindSynchronizationRules(containers: Set[ConstructionContainer]): Unit = {
    SynchronizationCompartment.getSyncRules().foreach { s =>
      var sync: ISyncCompartment = null
      //Proof all container for integration
      containers.foreach { cc =>
        if (s.isNextIntegration(cc.getPlayerInstance)) {
          if (cc.isConstructed && sync == null) {
            sync = s.getNewInstance
          }
          if (sync != null) {
            cc.getManagerInstance() play sync.getNextIntegrationRole(cc.getPlayerInstance())
          }
        }
      }
      if (sync != null)
        SynchronizationCompartment combine sync
    }
  }

  /**
   * Fill the test lists with all Players from the ConstructionContainers.
   */
  protected def fillTestLists(containers: Set[ConstructionContainer]): Unit = {
    containers.foreach { cc =>
      ModelElementLists.addElement(cc.getPlayerInstance)
    }
  }

  protected def makeCompleteConstructionProcess(containers: Set[ConstructionContainer]): Unit = {
    //first synchronize new compartments
    this.synchronizeCompartments(containers)

    //add role manager and relations
    this.addManagerRoles(containers)
    this.addRelatedRoleManager(containers)

    //binding of roles
    //this.addDeleteRoles(containers)
    this.bindSynchronizationRules(containers)
    this.addExtensionRoles(containers)

    //notify extensions
    this.notifyExtensionRoles(containers)

    //fill test list
    this.fillTestLists(containers)

    /*println("Construction ++++++++++++++++++++++++++++++++++++++++++++------------------------++++++++++++++++++++++++++++++++++++++++++++++++++++")
    containers.foreach { cc =>
      println((cc.getPlayerInstance).roles())
    }
    println("Construction ++++++++++++++++++++++++++++++++++++++++++++------------------------++++++++++++++++++++++++++++++++++++++++++++++++++++")*/
  }

}
