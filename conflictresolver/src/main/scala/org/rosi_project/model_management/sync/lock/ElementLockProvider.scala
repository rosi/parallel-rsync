package org.rosi_project.model_management.sync.lock

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashMap
import util.control.Breaks._
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.sql.Timestamp
import org.rosi_project.model_management.core.PlayerSync
import org.rosi_project.model_management.core.ModelElementLists
import org.rosi_project.model_management.util.UuidUtil
import scroll.internal.Compartment

import scala.collection.convert.ImplicitConversions.{`collection AsScalaIterable`, `map AsScalaConcurrentMap`}
import java.util.concurrent.atomic.AtomicInteger
import scala.sys.error

case class LockResult(success: Boolean, lock: ElementLock, wasDeleted: Boolean)
case class ElementListLockResult(lock: ElementLock, elements: ListBuffer[SynchronizationAware])

/**
 * This singleton creates and removes locks. All lock-modifying methods are synchronizes for threadsafety.
 * More informations about synchronized-methods: https://twitter.github.io/scala_school/concurrency.html
 */
object ElementLockProvider {

  val logger = LoggerFactory.getLogger(ElementLockProvider.getClass)

  /**
   * The configurable locking-timeout.
   */
  var TIMEOUT = 0

  /**
   * Checks if a lock is stale (is older then the locking-timeout).
   */
  def lockIsStale(lock: ElementLock): Boolean = {

    if (lock == null) {
      return true
    }

    var lockTime: Long = lock.getCreationDate()
    var isStale = (System.currentTimeMillis() - lockTime) > TIMEOUT

    if (isStale) {
      logger.info("Found stale lock with id: " + lock.getId())
    }

    return isStale
  }

  /**
   * Generates a new ElementLock-Object.
   */
  def provideLockObject(): ElementLock = new ElementLock(UuidUtil.generateUuid(), System.nanoTime())

  /**
   * Synchronized method for providing a new ElementLock on a element.
   * returns -> lock-success, lock if lock-success, flag if element was deleted
   */
  def provideLock(guid: String): LockResult = provideLockWithId(guid, UuidUtil.generateUuid())

  /**
   * Synchronized method for providing a new ElementLock with a specific id on a element.
   * returns -> lock-success, lock if lock-success, flag if element was deleted
   */
  def provideLockWithId(guid: String, id: String): LockResult = {

    this.synchronized {
      var player: SynchronizationAware = ModelElementLists.getElementByGuid(guid).asInstanceOf[SynchronizationAware]

      if (player == null) {
        return LockResult(success = false, null, wasDeleted = true)
      }

      val lock: ElementLock = new ElementLock(id, System.nanoTime())
      var alreadyLocked: Boolean = false

      player.getRelatedObjects().foreach { related =>
        var p: SynchronizationAware = related.asInstanceOf[SynchronizationAware]
        try {
          if (p.lock != null && p.lock.getId() != id && !lockIsStale(p.lock)) {
            alreadyLocked = true
          }
        } catch {
          case e: NullPointerException => alreadyLocked = true
        }
      }

      if (!alreadyLocked) {
        player.getRelatedObjects().foreach { related =>
          var p: SynchronizationAware = related.asInstanceOf[SynchronizationAware]
          p.lock = lock
          logger.info("Created new lock with id " + lock.getId() + " for element " + p.guid)
        }
      }
      if (!alreadyLocked) {
        return LockResult(success = true, lock, wasDeleted = false)
      } else {
        return LockResult(success = false, null, wasDeleted = false)
      }
    }
  }

  /**
   * Synchronized method for providing a new ElementLock with a specific id on a list of elements in a specific model given by its key.
   * returns -> lock if lock-success, locked elements if lock-success
   */
  def provideLockWithIdForElementList(elements: Set[String], id: String): ElementListLockResult = {

    this.synchronized {

      var lock: ElementLock = new ElementLock(id, System.nanoTime())
      var referencedElementsToBeLocked: ListBuffer[SynchronizationAware] = new ListBuffer[SynchronizationAware]()

      elements.foreach { e =>
        var lockElement: SynchronizationAware = ModelElementLists.getElementByGuid(e).asInstanceOf[SynchronizationAware]

        if (lockElement != null) {
          lockElement.getRelatedObjects().foreach { related =>
            try {
              if (related.asInstanceOf[SynchronizationAware].lock != null && related.asInstanceOf[SynchronizationAware].lock.getId() != id
                && !lockIsStale(related.asInstanceOf[SynchronizationAware].lock)) {
                return ElementListLockResult(null, ListBuffer())
              } else {
                referencedElementsToBeLocked += related.asInstanceOf[SynchronizationAware]
              }
            } catch {
              case e: NullPointerException => return ElementListLockResult(null, ListBuffer())
            }
          }
        }
      }

      referencedElementsToBeLocked.foreach { elem =>
        elem.lock = lock
      }

      return ElementListLockResult(lock, referencedElementsToBeLocked)
    }
  }

  /**
   * Synchronized method for providing a new ElementLock  on a element and its references.
   * returns -> lock-success, lock if lock-success, flag if referencing element was deleted
   */
  def provideLockForElements(referencingElementGuid: String, referencingElementModel: String,
                             elements: Map[String, Set[String]], id: String): LockResult = {
    return provideLockWithIdForElements(referencingElementGuid, referencingElementModel, elements, id)
  }

  /**
   * Synchronized method for providing a new ElementLock with a specific id on a element and its references.
   * returns -> lock-success, lock if lock-success, flag if referencing element was deleted
   */
  def provideLockWithIdForElements(referencingElementGuid: String, referencingElementModel: String,
                                   elements: Map[String, Set[String]], id: String): LockResult = {
    this.synchronized {

      var lock: ElementLock = new ElementLock(id, System.nanoTime())
      var foundReferencingElement: Boolean = false
      var referencedElementsToBeLocked: Set[SynchronizationAware] = Set[SynchronizationAware]()

      // check referencing element
      var referencingModelElement: SynchronizationAware = ModelElementLists.getElementByGuid(referencingElementGuid).asInstanceOf[SynchronizationAware]

      if (referencingModelElement != null) {
        referencingModelElement.getRelatedObjects().foreach { related =>
          var p: SynchronizationAware = related.asInstanceOf[SynchronizationAware]
          if (p.lock != null && p.lock.getId() != id && !lockIsStale(p.lock)) {
            return LockResult(success = false, null, wasDeleted = false)
          } else {
            referencedElementsToBeLocked += p
          }
        }
        foundReferencingElement = true
      }

      if (!foundReferencingElement) {
        return LockResult(success = false, null, wasDeleted = true)
      }

      // check referenced elements
      // iterate model-element-types of elements
      for ((k, v) <- elements) {
        // iterate elements in models
        v.foreach { guid =>
          val player: SynchronizationAware = ModelElementLists.getElementByGuid(referencingElementGuid).asInstanceOf[SynchronizationAware]
          if (player != null) {
            // we found a referenced element
            player.getRelatedObjects().foreach { related =>
              var p: SynchronizationAware = related.asInstanceOf[SynchronizationAware]
              if (p.lock != null && p.lock.getId() != id && !lockIsStale(p.lock)) {
                logger.info("wrong elem: " + p.guid)
                return LockResult(success = false, null, wasDeleted = false)
              } else {
                referencedElementsToBeLocked += p
              }
            }
          }
        }
      }

      // lock referencing element and not deleted referenced elements
      referencedElementsToBeLocked.foreach { elem =>
        elem.lock = lock
      }
      return LockResult(success = true, lock, wasDeleted = false)
    }
  }

  private def unlock(typedElement: SynchronizationAware, lockId: String): Unit ={
    if (typedElement.lock != null && typedElement.lock.getId() == lockId) {
      typedElement.lock = null
      typedElement.getRelatedObjects().foreach { related =>
        logger.info("Unlocking element: " + related.asInstanceOf[SynchronizationAware].guid)
        related.asInstanceOf[SynchronizationAware].lock = null
      }
    }
  }

  /**
   * Removes locks with a specific if id from all elements in models with given keys.
   */
  def removeLocks(models: Set[String], lockId: String): Unit = {
    this.synchronized {
      models.foreach { m =>
        val elements: Set[SynchronizationAware] = ModelElementLists.getDirectElementsFromType(m)
        elements.foreach { e =>
          val typedElement: SynchronizationAware = e.asInstanceOf[SynchronizationAware]
          unlock(typedElement, lockId)
        }
      }
    }
  }

  /**
   * Removes locks with a specific if id from all elements in all models
   */
  def removeLocksInAllModels(lockId: String): Unit = {
    this.synchronized {
      ModelElementLists.elements.forEach { (_, elements) =>
        elements.values().foreach { element =>
          val typedElement: SynchronizationAware = element.asInstanceOf[SynchronizationAware]
          unlock(typedElement, lockId)
        }
      }
    }
  }

  private def _removeLockFromElement(lockId: String, guid: String): Unit ={
    val typedElement: SynchronizationAware = ModelElementLists.getElementByGuid(guid).asInstanceOf[SynchronizationAware]
    if (typedElement != null && typedElement.lock != null && typedElement.lock.getId() == lockId && typedElement.guid == guid) {
      unlock(typedElement, lockId)
    }
  }

  /**
   * Removes lock with a specific id from a element given by its guid.
   */
  def removeLocksFromElement(lockId: String, guid: String): Unit = {
    this.synchronized {
      _removeLockFromElement(lockId, guid)
    }
  }

  /**
   * Removes a lock with a specific id from a set of elements given by their guid
   */
  def removeLocksFromElements(lockId: String, guids: Set[String]): Unit = {
    this.synchronized {
      guids.foreach(guid => {
        _removeLockFromElement(lockId, guid)
      })
    }
  }
}