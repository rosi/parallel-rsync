package org.rosi_project.model_management.sync.lock

import org.rosi_project.model_management.core.PlayerSync

/**
 * Integrates locks, GUID's and owners into all elements of all models.
 */
abstract class SynchronizationAware(var lock: ElementLock, val guid: String, var owner: String) extends PlayerSync {
  def getOwner(): String = owner

  def setOwner(s: String): Unit ={
    owner = s
  }

  def getLock(): ElementLock = lock

  def setLock(lock: ElementLock): Unit ={
    this.lock = lock
  }

  def getGuid(): String = guid
}