package org.rosi_project.model_management.sync.conflict.customrules

import org.rosi_project.model_management.core.PlayerSync

/**
 * Interface for all custom reference-rules.
 */
trait ICustomReferenceRule {
  
  /**
   * get name of this rule
   */
  def getRuleName(): String
  
  /**
   * flag that defines if its a reference-creation- or a reference-deletion-rule
   */
  def isReferenceDeletionRule(): Boolean
  
  /**
   * true if referencing / referenced element matches the rule
   */
  def matchesRule(referencingElement: PlayerSync, referencedElement: PlayerSync, senderRv: Int, senderOwner: String, collection: String): Boolean
}