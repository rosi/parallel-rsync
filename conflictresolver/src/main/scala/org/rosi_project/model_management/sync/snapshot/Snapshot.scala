package org.rosi_project.model_management.sync.snapshot

/**
 * A snapshot of the meta-information of a specific element with GUID of type elementKey.
 */
case class Snapshot(guid: String, ts: Int, rv: Int, elementKey: String)