package org.rosi_project.model_management.sync.procedures.data

/**
 * Base-class for all data-change-records for CCR and SCR.
 */
abstract class ChangeRecord(var abstractTs: Int, var abstractRv: Int, var abstractGuid: String, 
    var abstractRefOnly: Boolean, var owner: String) {
  
}