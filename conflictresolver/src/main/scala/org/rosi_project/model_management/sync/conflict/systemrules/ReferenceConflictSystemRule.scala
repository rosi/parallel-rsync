package org.rosi_project.model_management.sync.conflict.systemrules

import org.rosi_project.model_management.core.PlayerSync

/**
 * Interface for all reference-conflict-system-rules.
 */
trait ReferenceConflictSystemRule {
  
  /**
   * returns the rule name
   */
   def getRuleName(): String
  
  // ADD-REF & DEL-REF  
  /**
   * resolves conflicts of type: deleted referenced element
   * guid -> id of referenced element
   */
  def resolveDeletedReferencedElementConflicts(guid: String, elementKey: String): PlayerSync
  
  // REF-MOD-DEL & REF-MOD-ADD 
  /**
   * resolves conflicts of type: deleted referencing element
   * guid -> id of referencing element
   */
  def resolveDeletedReferencingElementConflicts(guid: String, elementKey: String): PlayerSync
  
  // REF-COUNTER (replaced by custom rules)
  @deprecated
  def resolveReferenceCounterConflicts(rv: Int, referencingElement: PlayerSync): Boolean
  
}