package org.rosi_project.model_management.sync.procedures.data

import scala.collection.mutable.ListBuffer

/**
 * Base-class for all implementations of SCR.
 */
abstract class ServerChangeRecordList {}