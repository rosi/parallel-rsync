package org.rosi_project.model_management.sync.procedures.data

/**
 * Base-class for entries of SCR which represent reference-changes.
 */
abstract class IServerReferenceChangeRecord {}