package org.rosi_project.model_management.sync.procedures.data

/**
 * Entry of CER.
 */
case class ClientEntireRecord(guid: String, ts: Int, rv: Int, elementType: String)