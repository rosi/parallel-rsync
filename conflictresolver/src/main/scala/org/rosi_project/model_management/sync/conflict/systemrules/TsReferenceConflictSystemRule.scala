package org.rosi_project.model_management.sync.conflict.systemrules

import org.rosi_project.model_management.core.PlayerSync
import org.rosi_project.model_management.core.ModelElementLists
import org.rosi_project.model_management.sync.lock._

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Reference-conflict-system-rule which integrates the reference-version mechanism.
 */
@SystemRule("reference")
class TsReferenceConflictSystemRule extends ReferenceConflictSystemRule {

  val logger = LoggerFactory.getLogger(classOf[TsReferenceConflictSystemRule])
  
  def getRuleName() = "TsReferenceConflictSystemRule"

  def resolveDeletedReferencedElementConflicts(guid: String, elementKey: String): PlayerSync = {

    var elementSet: Set[SynchronizationAware] = ModelElementLists.getDirectElementsFromType(elementKey)

    elementSet.foreach { elem =>
      var playerSync: SynchronizationAware = elem.asInstanceOf[SynchronizationAware]

      // check for conflicts
      if (playerSync.guid == guid) {
        if (playerSync.deleted) {
          logger.warn("Detected deleted element in ModelElementsList ... this should not occur!")
        } else {
          return playerSync
        }
      }
    }
    logger.info("Detected ref-conflict (ADD-REF/DEL-REF): " + guid)
    return null
  }

  def resolveDeletedReferencingElementConflicts(guid: String, elementKey: String): PlayerSync = {

    var elementSet: Set[SynchronizationAware] = ModelElementLists.getDirectElementsFromType(elementKey)

    elementSet.foreach { elem =>
      var playerSync: SynchronizationAware = elem.asInstanceOf[SynchronizationAware]

      if (playerSync.guid == guid) {
        if (playerSync.deleted) {
          logger.warn("Detected deleted element in ModelElementsList ... this should not occur!")
        } else {
          return playerSync
        }
      }
    }
    logger.error("Detected ref-conflict (REF-MOD-DEL/REF-MOD-ADD): " + guid)
    return null
  }
  
  // true --> we take new refs
  @deprecated
  def resolveReferenceCounterConflicts(rv: Int, referencingElement: PlayerSync): Boolean = {
    // reason for 2nd condition: initially the ref-version of a new element is set to 1
    return rv == referencingElement.rv || (rv == 0 && referencingElement.rv == 1)
  }
}