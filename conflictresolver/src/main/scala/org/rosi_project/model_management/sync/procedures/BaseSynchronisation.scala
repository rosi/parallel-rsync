package org.rosi_project.model_management.sync.procedures

import scala.collection.mutable.ListBuffer

import org.rosi_project.model_management.sync.lock._

import org.rosi_project.model_management.sync.procedures.data.ServerChangeRecordList
import org.rosi_project.model_management.sync.procedures.data._

/**
 * Base class for all synchronization-algorithms (phase of applying updates from clients).
 */
trait BaseSynchronisation {

  /**
   * Synchronizes updates from the unparsed synchronization-request.
   */
  def syncModelToHub(request: String): ServerChangeRecordList

  /**
   * Synchronizes new elements and uses the lock-object to lock them.
   */
  def syncNewElementsToHub(request: String, lock: ElementLock): Map[String, String]

  /**
   * Synchronizes reference-modifications and uses the lock-object to lock referencing and referenced elements.
   */
  def syncReferenceModsToHub(records: ListBuffer[ListBuffer[_ <: ChangeRecord]], idMapping: Map[String, String], 
      lock: ElementLock): (Int, ListBuffer[ListBuffer[_ <: ChangeRecord]], Set[String])

  /**
   * Synchronizes element-data-modifications and uses the lock-object to lock them elements to modify.
   */
  def syncModificationsToHub(records: ListBuffer[ListBuffer[_ <: ChangeRecord]], lock: ElementLock):
      (Int, ListBuffer[ListBuffer[_ <: ChangeRecord]])

  /**
   * Synchronizes element-deletions and uses the lock-object to lock the elements to be deleted.
   */
  def syncDeletionToHub(records: ListBuffer[ListBuffer[_ <: ChangeRecord]], lock: ElementLock):
      (Int, ListBuffer[ListBuffer[_ <: ChangeRecord]])

}