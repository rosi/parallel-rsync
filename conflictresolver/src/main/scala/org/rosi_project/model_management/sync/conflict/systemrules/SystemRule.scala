package org.rosi_project.model_management.sync.conflict.systemrules

/**
 * annotation for system-rules, to specify if a rule is a reference- or a data-rule
 */
class SystemRule(resolverType: String) extends scala.annotation.StaticAnnotation{
  
}