package org.rosi_project.model_management.sync.lock

import java.sql.Timestamp
import java.util.Date

/**
 * Represents a lock, which can be attached to an element.
 */
class ElementLock(protected val id: String, protected val creationDate: Long) {
  
  /**
   * returns the id of the lock
   */
  def getId() : String = id
  
  /**
   * returns the creation-date of the lock
   */
  def getCreationDate() : Long = creationDate

  override def toString(): String = "ElementLock: [id:" + id + "] [creationDate: " + new Date(creationDate) + "]"
}