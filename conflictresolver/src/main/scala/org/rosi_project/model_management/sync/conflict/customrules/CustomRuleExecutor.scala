package org.rosi_project.model_management.sync.conflict.customrules

import org.rosi_project.model_management.core.PlayerSync

import scala.collection.mutable.ListBuffer

import org.slf4j.LoggerFactory

/**
 * Allows the execution of a row custom rules combined by logical OR / AND.
 */
object CustomRuleExecutor {
  
  private val logger = LoggerFactory.getLogger(CustomRuleExecutor.getClass)
  private var refRules: ListBuffer[ICustomReferenceRule] = new ListBuffer[ICustomReferenceRule]()
  private var deletionRules: ListBuffer[ICustomDeletionRule] = new ListBuffer[ICustomDeletionRule]()
  
  /**
   * returns all custom reference rules
   */
  def getRefRules(): ListBuffer[ICustomReferenceRule] = {
    return refRules
  }
  
  /**
   * returns all custom deletion rules
   */
  def getDeletionRules(): ListBuffer[ICustomDeletionRule] = {
    return deletionRules
  }
  
  /**
   * logs out all rules that the CustomRuleExecutor knows
   */
  def logRules(): Unit = {
    logger.debug("*** Custom Reference Rules:")
    
    refRules.foreach{ r =>
      logger.debug("****** " + r.getRuleName())
    }
    
    logger.debug("*** Custom Deletion Rules:")
    
    deletionRules.foreach{ r =>
      logger.debug("****** " + r.getRuleName())
    }
  }

  /**
   * adds a custom deletion rule to the CustomRuleExecutor
   */
  def addCustomDeletionRule(rule: ICustomDeletionRule): Unit = {
    deletionRules.foreach { r =>
      if (r.getRuleName() == rule.getRuleName()) {
        return
      }
    }
    deletionRules += rule
  }

  /**
   * removes a custom deletion rule from the CustomRuleExecutor
   */
  def removeCustomDeletionRule(ruleName: String): Unit = {
    deletionRules.foreach { r =>
      if (r.getRuleName() == ruleName) {
        deletionRules -= r
        return
      }
    }
  }

  /**
   * adds a custom reference rule to the CustomRuleExecutor
   */
  def addCustomReferenceRule(rules: ICustomReferenceRule*): Unit = {
    rules.foreach { rule =>
      refRules.foreach { r =>
        if (r.getRuleName() == rule.getRuleName()) {
          return
        }
      }
      refRules += rule
    }
  }

  /**
   * removes a custom reference rule from the CustomRuleExecutor
   */
  def removeCustomReferenceRule(ruleName: String): Unit = {
    refRules.foreach { r =>
      if (r.getRuleName() == ruleName) {
        refRules -= r
        return
      }
    }
  }

  /**
   * applies reference rules (connected with logical AND)
   */
  def matchesAllCustomReferenceRules(referencingElement: PlayerSync, referencedElement: PlayerSync, senderRv: Int, isDeletion: Boolean, senderOwner: String, collection: String): Boolean = {

    refRules.foreach { r =>
      if (r.isReferenceDeletionRule() == isDeletion) {
        logger.info("####### Executing rule: " + r.getRuleName())
        if (!r.matchesRule(referencingElement, referencedElement, senderRv, senderOwner, collection)) {
          return false
        }
      }
    }
    return true
  }

  /**
   * applies reference rules (connected with logical OR)
   */
  def matchesMinOneCustomReferenceRule(referencingElement: PlayerSync, referencedElement: PlayerSync, senderRv: Int, 
      isDeletion: Boolean, senderOwner: String, collection: String): Boolean = {

    if (refRules.isEmpty) {
      return true
    }

    refRules.foreach { r =>
      if (r.isReferenceDeletionRule() == isDeletion) {
        if (r.matchesRule(referencingElement, referencedElement, senderRv, senderOwner, collection)) {
          return true
        }
      }
    }
    return false
  }

  /**
   * applies deletion rules (connected with logical AND)
   */
  def matchesAllCustomDeletionRules(element: PlayerSync, senderOwner: String): Boolean = {

    deletionRules.foreach { r =>
      if (!r.matchesRule(element, senderOwner)) {
        return false
      }
    }
    return true
  }

  /**
   * applies deletion rules (connected with logical OR)
   */
  def matchesMinOneCustomDeletionRule(element: PlayerSync, senderOwner: String): Boolean = {

    if (refRules.size == 0) {
      return true
    }

    deletionRules.foreach { r =>
      if (r.matchesRule(element, senderOwner)) {
        return true
      }
    }
    return false
  }
}