package org.rosi_project.model_management.sync.roles

/**
 *  PART OF RSYNC-CORE by Christopher Werner (TUD)
  * Interface for the destructor roles.
  */
trait IDestructor {
  
  /**
    * General destruction function for external call.
    */
  def deleteRoleFunction(): Unit
}
