package org.rosi_project.model_management

import org.rosi_project.model_management.core.{ConflictResolvable, ModelElementLists, ModelInformation, PlayerSync}
import org.rosi_project.model_management.sync.lock.{ElementLock, ElementLockProvider, LockResult, SynchronizationAware}
import org.rosi_project.model_management.sync.procedures.BaseResponseGeneration
import org.rosi_project.model_management.sync.procedures.data.{ClientEntireRecord, IServerReferenceChangeRecord}
import org.rosi_project.model_management.sync.snapshot.{Snapshot, SnapshotProvider}
import org.slf4j.LoggerFactory

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks.{break, breakable}

object ResponseBuilder extends BaseResponseGeneration {

  val logger = LoggerFactory.getLogger(classOf[PlayerSync])

  def buildReferenceChanges(snapshot: ListBuffer[Snapshot], idMapping: Map[String, String], cer: Seq[ClientEntireRecord], notUpdatedOnServer: Set[String], lock: ElementLock,
                            records: mutable.Map[String, ListBuffer[ServerChangeRecord]], referenceRecords: ListBuffer[ServerReferenceChangeRecord],
                            processedReferencingElements: ListBuffer[String], owner: String): Int = {
    var lockFailCounter: Int = 0

    snapshot.foreach { sn =>
      var found = false
      if(!processedReferencingElements.contains(sn.guid)){
        breakable {
          cer.foreach { ic =>
            val realGuid = idMapping.getOrElse(ic.guid, ic.guid)
            val element = ModelElementLists.getElementByGuid(realGuid).asInstanceOf[ConflictResolvable]

            if(sn.guid == realGuid){
              found = true
              if(sn.rv > getCerRv(cer, realGuid) || notUpdatedOnServer.contains(realGuid)){
                val lockResult = ElementLockProvider.provideLockWithId(realGuid, lock.getId())
                if(lockResult.wasDeleted){
                  processedReferencingElements += realGuid
                  break
                }
                if(lockResult.success && !lockResult.wasDeleted){
                  cleanRefs(sn.guid, sn.elementKey)
                  val refs: Set[String] = element.getAssociated.values.flatten.toSet[ConflictResolvable].map(x => x.guid)
                  val refLockRefs = ElementLockProvider.provideLockWithIdForElementList(refs, lock.getId())

                  // Handle results of reference-locks
                  if(refLockRefs.lock == null){
                    // Lock failed on a minimum of 1 referenced element
                    lockFailCounter += 1
                    ElementLockProvider.removeLocksInAllModels(lock.getId())
                    break
                  } else {
                    // Lock Success
                    val refRecordRefs: ListBuffer[String] = ListBuffer()
                    var recordRvDemotion: Boolean = false
                    var recordRv: Int = 0

                    // Integrate references in response
                    refLockRefs.elements.foreach { ref =>
                      if(SnapshotProvider.snapshotContains(snapshot, ref.guid)){
                        refRecordRefs += ref.guid
                      } else {
                        recordRvDemotion = true
                      }
                    }

                    if(!recordRvDemotion){
                      recordRv = sn.rv
                    }
                    referenceRecords += ServerReferenceChangeRecord(element.getClass.getName, realGuid, sn.ts, recordRv, refRecordRefs)
                    processedReferencingElements += realGuid
                  }
                }

                if(!lockResult.success && lockResult.lock == null && !lockResult.wasDeleted){
                  // No lock-success, do nothing
                  lockFailCounter += 1
                  break
                }

                if(realGuid == sn.guid){
                  ElementLockProvider.removeLocksInAllModels(lock.getId())
                  break
                }
              }
            }
          }

          if(!found){
            if(!integrateNewRefsFromNewElements(sn, snapshot, lock, processedReferencingElements, referenceRecords)){
              lockFailCounter += 1
            }
          }
        }
      }
    }

    lockFailCounter
  }

  private def cleanRefs(guid: String, model: String): Unit = {

    val elementSet: Set[SynchronizationAware] = ModelElementLists.getDirectElementsFromType(model)
    val refsToRemove: ListBuffer[ConflictResolvable] = new ListBuffer[ConflictResolvable]()
    var element: ConflictResolvable = null

    breakable {
      elementSet.foreach { e =>
        element = e.asInstanceOf[ConflictResolvable]

        if (element.guid == guid) {
          element.getRelatedObjects().foreach { refd =>
              if (refd == null || refd.isDeleted) {
                logger.info("Found invalide ref... queueing it for removal.")
                refsToRemove += refd.asInstanceOf[ConflictResolvable]
            }
          }
          break
        }
      }
    }

    refsToRemove.foreach { r =>
      element.removeAssociation(r)
    }
  }


  private def integrateNewRefsFromNewElements(sn: Snapshot, snapshot: ListBuffer[Snapshot], lock: ElementLock, processedReferencingElements: ListBuffer[String],
                                              referenceRecords: ListBuffer[ServerReferenceChangeRecord]): Boolean = {
    val lockRes: LockResult = ElementLockProvider.provideLockWithId(sn.guid, lock.getId())

    if (lockRes.wasDeleted) {
      processedReferencingElements += sn.guid
    }

    if (lockRes.success && !lockRes.wasDeleted) {
      val element = ModelElementLists.getElementByGuid(sn.guid).asInstanceOf[ConflictResolvable]
      val associated = element.getAssociated
      val refs: Set[String] = associated.values.flatten.map(x => x.guid).toSet
      val refLockRes = ElementLockProvider.provideLockWithIdForElementList(refs, lock.getId())

      if (refLockRes.lock == null) {
        ElementLockProvider.removeLocksInAllModels(lock.getId())
        return false
      } else {
        val refRecordRefs: ListBuffer[String] = ListBuffer()
        var recordRvDemotion: Boolean = false
        var recordRv: Int = 0

        refLockRes.elements.foreach { ref =>
          if (SnapshotProvider.snapshotContains(snapshot, ref.guid)) {
            refRecordRefs += ref.guid
          } else {
            recordRvDemotion = true
          }
        }

        if (!recordRvDemotion) {
          recordRv = sn.rv
        }
        referenceRecords += ServerReferenceChangeRecord(sn.elementKey, sn.guid, sn.ts, recordRv, refRecordRefs)
        processedReferencingElements += sn.guid
        ElementLockProvider.removeLocksInAllModels(lock.getId())
        return true
      }
    }

    ElementLockProvider.removeLocksInAllModels(lock.getId())
    false
  }

  def buildResponse(models: Set[String], idMapping: Map[String, String], cer: Seq[ClientEntireRecord], notUpdatedOnServer: Set[String], owner: String): mutable.Map[String, ListBuffer[ServerChangeRecord]] = {
    val responseLock = ElementLockProvider.provideLockObject()

    val snapshot0 = SnapshotProvider.provideSnapshot(models)
    val snapshot1 = SnapshotProvider.copySnapshot(snapshot0)

    val changeRecords: mutable.Map[String, ListBuffer[ServerChangeRecord]] = mutable.Map[String, ListBuffer[ServerChangeRecord]]().withDefault(_ => ListBuffer())
    val changeRefRecords: mutable.Map[String, ListBuffer[ServerReferenceChangeRecord]] = mutable.Map[String, ListBuffer[ServerReferenceChangeRecord]]().withDefault(_ => ListBuffer())
    val processedReferencingElements: ListBuffer[String] = ListBuffer[String]()

    var dataRetryNeeded = true
    while(dataRetryNeeded){
      val dataLockFails: Int = buildDataChanges(snapshot0, idMapping, cer, notUpdatedOnServer, responseLock, changeRecords, owner)
      if(dataLockFails == 0) dataRetryNeeded = false
    }

    var refRetryNeeded = true
    while(refRetryNeeded){
      val refLockFails: Int = buildReferenceChanges(snapshot1, idMapping, cer, notUpdatedOnServer, responseLock, changeRecords,
        ListBuffer(changeRefRecords.values.toSeq.flatten:_*), processedReferencingElements, owner)
      if(refLockFails == 0) refRetryNeeded = false
    }

    // ElementLockProvider.removeLocksInAllModels(responseLock.getId())

    changeRecords
  }

  def buildDataChanges(snapshot: ListBuffer[Snapshot], idMapping: Map[String, String], cer: Seq[ClientEntireRecord], notUpdatedOnServer: Set[String],
                       lock: ElementLock, records: mutable.Map[String, ListBuffer[ServerChangeRecord]], owner: String): Int = {
    var lockFailCounter: Int = 0

    snapshot.foreach { sn =>
      var found: Boolean = false

      breakable {
        cer.foreach { r =>
          val realGuid = idMapping.getOrElse(r.guid, r.guid)

          // Check if element needs to be locked (if there are changes)
          if(realGuid == sn.guid && sn.ts > r.ts){
            val lockResult: LockResult = ElementLockProvider.provideLockWithId(realGuid, lock.getId())
            if(lockResult.success && !lockResult.wasDeleted){
              val element = ModelElementLists.getElementByGuid(realGuid).asInstanceOf[ConflictResolvable]
              val elements = records(r.elementType)
              elements.append(ServerChangeRecord(r.elementType, realGuid, element.ts, element.rv, refonly = false, realGuid, element.owner, element.serialize()))
              records.put(r.elementType, elements)
              snapshot -= sn
            }
            if(lockResult.wasDeleted){
              val elements = records(r.elementType)
              elements.append(ServerChangeRecord(r.elementType, r.guid, 0, 0, refonly = false, "", "", Map()))
              records.put(r.elementType, elements)
              snapshot -= sn
            }
            if(!lockResult.success && lockResult.lock == null && !lockResult.wasDeleted){
              lockFailCounter += 1
            }
          }

          if(realGuid == sn.guid){
            found = true
            break
          }
        }
      }

      // New element
      if(!found) {
        val element = ModelElementLists.getElementByGuid(sn.guid).asInstanceOf[ConflictResolvable]

        if(element.owner == owner) {
          val elementType = element.getClass.getName
          val elements = records(elementType)
          elements.append(ServerChangeRecord(element.getClass.getName, element.guid, element.ts, element.rv, refonly = false, "", element.owner, element.serialize()))
          records.put(elementType, elements)
        }
        snapshot -= sn
      }
    }

    ElementLockProvider.removeLocksInAllModels(lock.getId())

    cer.foreach { r =>
      if(r.ts != 0){
        val element = ModelElementLists.getElementByGuid(r.guid)
        if(element == null){
          val elements = records(r.elementType)
          elements.append(ServerChangeRecord(r.elementType, r.guid, 0, 0, refonly = false, "", "", Map()))
          records.put(r.elementType, elements)
        }
      }
    }

    lockFailCounter
  }

  def getCerRv(cer: Seq[ClientEntireRecord], guid: String): Int = {
    val element = cer.find(r => r.guid == guid)

    if(element.isDefined) element.get.rv else 0
  }
}
