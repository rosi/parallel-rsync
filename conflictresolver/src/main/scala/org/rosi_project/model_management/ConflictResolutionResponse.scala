package org.rosi_project.model_management

import org.rosi_project.model_management.util.PerformanceInformation

import scala.collection.mutable
import scala.collection.mutable.ListBuffer


case class ConflictResolutionResponse(changes: Seq[ServerChangeRecord], performance: PerformanceInformation)
