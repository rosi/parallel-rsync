package org.rosi_project.model_management

import org.rosi_project.model_management.core.{ConflictResolvable, ModelElementLists, ModelInformation, PlayerSync}
import org.rosi_project.model_management.sync.conflict.customrules.CustomRuleExecutor
import org.rosi_project.model_management.sync.conflict.systemrules.SystemRuleExecutor
import org.rosi_project.model_management.sync.lock.{ElementLock, ElementLockProvider}
import org.rosi_project.model_management.sync.procedures.data.ClientEntireRecord
import org.rosi_project.model_management.util.{DuplicateValidator, PerformanceCounter, PerformanceInformation, UuidUtil}
import org.slf4j.LoggerFactory
import scroll.internal.Compartment

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.reflect.classTag
import scala.sys.error

case class SynchronizationResult(lockFailCounter: Int, deletions: Seq[ConflictResolvable])

case class ReferenceResolutionResult(lockFailCounter: Int, notUpdatedOnServer: Set[String])

case class ApplyReferenceResult(successes: Int, notUpdatedOnServer: Set[String])

object ConflictResolution {
  val logger = LoggerFactory.getLogger(getClass)

  private def applyRefDeletions(targetObject: ConflictResolvable, targetId: String, change: ClientChangeRecord, idMapping: mutable.Map[String, String]): ApplyReferenceResult = {
    var successCounter: Int = 0
    var notUpdatedOnServer: Set[String] = Set()

    val serialized = mutable.Map(targetObject.serialize.toSeq:_*)

    change.deletedReferencesTuples.foreach { case (fieldName, oref) =>
      val guid = idMapping.getOrElse(oref, oref)
      val fieldType = ModelInformation.getAssociationFieldType(change.model, fieldName)
      val referencedObject = SystemRuleExecutor.resolveDeletedReferencedElementReferenceConflicts(guid, fieldType).asInstanceOf[ConflictResolvable]

      if (referencedObject != null && CustomRuleExecutor
        .matchesAllCustomReferenceRules(targetObject, referencedObject, change.rv, isDeletion = true, change.owner, fieldName)) {
        logger.info(s"removed ref from ${change.model}: ${targetObject.guid}")

        var guidList = serialized(fieldName).split('|').to[Set]
        guidList -= referencedObject.guid
        serialized.put(fieldName, guidList.mkString ("|"))

        successCounter = successCounter + 1
      } else {
        notUpdatedOnServer += targetId
      }
    }

    targetObject.unserialize(serialized.toMap.withDefaultValue(""))
    ApplyReferenceResult(successCounter, notUpdatedOnServer)
  }

  private def applyRefAdditions(targetObject: ConflictResolvable, targetId: String, change: ClientChangeRecord, idMapping: mutable.Map[String, String]): ApplyReferenceResult = {
    var successCounter: Int = 0
    var notUpdatedOnServer: Set[String] = Set()

    val serialized = mutable.Map(targetObject.serialize().toSeq:_*)

    change.addedReferencesTuples.foreach { case (fieldName, ref) =>
      val fieldType = ModelInformation.getAssociationFieldType(change.model, fieldName)
      val referencedId = idMapping.getOrElse(ref, ref)
      val referencedObject: PlayerSync = SystemRuleExecutor.resolveDeletedReferencedElementReferenceConflicts(referencedId, fieldType)

      if(referencedObject != null && CustomRuleExecutor.matchesAllCustomReferenceRules(targetObject, referencedObject, change.rv, isDeletion = false, change.owner, fieldName)){
        logger.info(s"added ref from ${change.model}: ${targetObject.guid}")

        var guidList = serialized(fieldName).split('|').to[Set]
        guidList += referencedId
        serialized.put(fieldName, guidList.mkString ("|"))

        successCounter = successCounter + 1
      } else {
        notUpdatedOnServer += targetId
      }
    }

    if(successCounter > 0){
      targetObject.rv += 1
    }

    ApplyReferenceResult(successCounter, notUpdatedOnServer)
  }


  private def syncDeletions(changes: Seq[ClientChangeRecord], lock: ElementLock, idMapping: mutable.Map[String, String]): SynchronizationResult = {
    var lockFailCounter: Int = 0
    val deletions: mutable.ListBuffer[ConflictResolvable] = ListBuffer()

    changes.foreach { change =>
      if (change.ts < 0) {
        val lockRes = ElementLockProvider.provideLockWithId(change.guid, lock.getId())
        if(lockRes.success || lockRes.wasDeleted){
          val res: (Boolean, PlayerSync) = SystemRuleExecutor.resolveDataDeletionConflicts(change.ts, change.owner, change.guid, change.model)

          if(res._1){
            val deleted: ConflictResolvable = res._2.asInstanceOf[ConflictResolvable]
            if(deleted.guid == change.guid){
              logger.info(s"Deleting ${change.model} with guid ${change.guid}")
              deleted.deleteObjectFromSynchro()
              deletions += deleted
            }
          }
        } else if(!lockRes.success || lockRes.lock == null || !lockRes.wasDeleted ) {
          lockFailCounter += 1
        }
      }
    }

    SynchronizationResult(lockFailCounter, deletions)
  }

  private def syncNewElements(changes: Seq[ClientChangeRecord], lock: ElementLock, idMapping: mutable.Map[String, String]): Set[ConflictResolvable] ={
    val newObjects = mutable.Set[ConflictResolvable]()

    changes.foreach(change => {
      if(change.ts == 0) {
          if (!idMapping.contains(change.lokalId)) {
            val model: String = change.model
            val guid: String = UuidUtil.generateUuid()

            val data = change.updatedFields.withDefaultValue("") + ("owner" -> change.owner)

            val newObject: ConflictResolvable = Class.forName(model)
              .getConstructor(classTag[String].runtimeClass, classTag[ElementLock].runtimeClass, classTag[Map[String, String]].runtimeClass)
              .newInstance(guid, lock, data).asInstanceOf[ConflictResolvable]

            newObject.ts = 1
            newObject.rv = 1

            idMapping += change.lokalId -> guid
            newObjects.add(newObject)
          } else
            logger.error("Ignored duplicate element: " + change.guid)
        }
    })

    newObjects.toSet
  }

  private def syncModifications(changes: ListBuffer[ClientChangeRecord], lock: ElementLock): SynchronizationResult = {
    var lockFailCounter = 0
    val modifications = ListBuffer[ConflictResolvable]()

    changes.foreach { change =>
      if(change.ts > 0 && !change.refonly){
        val lockRes = ElementLockProvider.provideLockWithId(change.guid, lock.getId())
        if(lockRes.success || lockRes.wasDeleted){
          val res = SystemRuleExecutor.resolveDataModificationConflicts(change.ts, change.owner, change.guid, change.model)
          if(res._1){
            val data: ConflictResolvable = res._2.asInstanceOf[ConflictResolvable]
            if(data.guid == change.guid){
              data.unserialize(change.updatedFields.withDefaultValue(""))
              data.owner = change.owner
              if(data.ts == Int.MaxValue){
                data.ts = 1
              } else {
                data.ts += 1
              }
              data.syncFieldsToOtherModels()
              modifications.append(data)
            }
          }
          changes -= change
          if(lockRes.success) ElementLockProvider.removeLocks(Set(change.model), lockRes.lock.getId())
        } else if(!lockRes.success && lockRes.lock == null && !lockRes.wasDeleted){
          lockFailCounter += 1
        }
      }
    }

    SynchronizationResult(lockFailCounter, modifications)
  }

  private def syncReferenceModsToHub(changes: ListBuffer[ClientChangeRecord], idMapping: mutable.Map[String, String], lock: ElementLock): ReferenceResolutionResult = {
    var notUpdatedOnServer: mutable.Set[String] = mutable.Set()
    var lockFailCounter: Int = 0

    changes.foreach(change => {
      if(change.ts > -1 && (change.addedReferences.nonEmpty || change.deletedReferences.nonEmpty)){
        val targetId = idMapping.getOrElse(change.lokalId, change.guid)

        val collections = (change.addedReferences.keys ++ change.deletedReferences.keys).toSet
        // Maps field names to type names
        val changedReferencedTypes: Map[String, String] = collections.map(field => (field, ModelInformation.getAssociationFieldType(change.model, field))).toMap
        // Maps type names to element guids
        val changedReferencedElements: Map[String, Set[String]] = change.changedReferences
          .map { case (name, id) => (changedReferencedTypes(name), id) }
          .groupBy(_._1).mapValues(ids => ids.map { case (_, id) => idMapping.getOrElse(id, id) }.toSet)
        // Lock referencing element and all changed referenced elements
        val lockRes = ElementLockProvider.provideLockWithIdForElements(targetId, change.model, changedReferencedElements, lock.getId())

        if(lockRes.success  || lockRes.wasDeleted){
          val targetPlayer: PlayerSync = SystemRuleExecutor.resolveDeletedReferencingElementReferenceConflicts(targetId, change.model)
          if(targetPlayer != null){
            logger.info(s"Passed ref-checks for ${change.model}")
            val targetObject = targetPlayer.asInstanceOf[ConflictResolvable]
            var overAllSuccess: Int = 0

            // refs can only be removed if the object is not new
            if(change.ts >= 0 && targetObject != null) {
              if (change.ts > 0) {
                val resDel = applyRefDeletions(targetObject, targetId, change, idMapping)
                overAllSuccess += resDel.successes
                notUpdatedOnServer ++= resDel.notUpdatedOnServer
              }
              val resAdd = applyRefAdditions(targetObject, targetId, change, idMapping)
              overAllSuccess += resAdd.successes
              notUpdatedOnServer ++= resAdd.notUpdatedOnServer

              if(overAllSuccess > 0){
                val currentRv: Int = targetObject.rv
                if(currentRv == Int.MaxValue){
                  targetObject.rv = 1
                } else targetObject.rv += 1
              }
            }
          }
          changes -= change
          if(lockRes.success) ElementLockProvider.removeLocksFromElement(lockRes.lock.getId(), targetId)
        } else if(!lockRes.success || lockRes.lock == null || !lockRes.wasDeleted){
          lockFailCounter += 1
        }
      }
    })

    if(notUpdatedOnServer.nonEmpty){
      logger.warn(s"Elements with invalid refs: ${notUpdatedOnServer}")
    }

    ReferenceResolutionResult(lockFailCounter, notUpdatedOnServer.to[Set])
  }

  def syncModel(name: String, changesSeq: Seq[ClientChangeRecord], entireRecords: Seq[ClientEntireRecord], elementCount: Int, owner: String, threadCount: Int = 1): ConflictResolutionResponse = {
    val startTime = System.nanoTime()
    var totalLockFailCounter = 0
    val lock: ElementLock = ElementLockProvider.provideLockObject()
    val guids: Seq[String] = changesSeq.map(x => x.guid)
    val changes: ListBuffer[ClientChangeRecord] = changesSeq.to[ListBuffer]
    val changesRef: ListBuffer[ClientChangeRecord] = changesSeq.to[ListBuffer]
    var notUpdatedOnServer: Set[String] = Set()
    val models: Set[String] = changes.map(x => x.model).toSet

    // Reject complete set of changes if any model is unknown
    models.foreach(x => {
      if(!ModelInformation.modelExists(x)) throw new IllegalArgumentException(s"Model ${x} is unknown")
    })

    // Reject complete set of changes if an element was modified twice (INS-INS)
    // if(DuplicateValidator.containsDuplicates(guids)) throw new IllegalArgumentException("List of changes contains multiple modifications of the same element")

    val idMapping = mutable.Map[String, String]()

    // Step 1: Create new Elements
    val newElements = synchronized {
      syncNewElements(changes, lock, idMapping)
    }

    // Step 2: Refs
    var refRetryNeeded = false
    do {
      val refModRes = syncReferenceModsToHub(changesRef, idMapping, lock)
      notUpdatedOnServer ++= refModRes.notUpdatedOnServer
      refRetryNeeded = refModRes.lockFailCounter > 0
      totalLockFailCounter += refModRes.lockFailCounter
    } while(refRetryNeeded)

    // Unlock remaining new elements
    ElementLockProvider.removeLocksFromElements(lock.getId(), newElements.map(x => x.guid))

    // Step 3: Modify
    var modRetryNeeded = false
    var modTime = System.nanoTime()
    do {
      val modRes = syncModifications(changes, lock)
      modRetryNeeded = modRes.lockFailCounter > 0
      totalLockFailCounter += modRes.lockFailCounter
    } while(modRetryNeeded)


    // Step 4: Deletions
    var delRetryNeeded = false
    do {
      val delRes = syncDeletions(changes, lock, idMapping)
      delRetryNeeded = delRes.lockFailCounter > 0
      totalLockFailCounter += delRes.lockFailCounter
    } while(delRetryNeeded)

    val time = System.nanoTime() - startTime
    val performanceInformation = PerformanceInformation(Thread.currentThread().getId, totalLockFailCounter, name, time, elementCount, threadCount)

    val response = ResponseBuilder.buildResponse(models, idMapping.toMap, entireRecords, notUpdatedOnServer, owner)
      .flatMap { case (_, xs) => xs }
      .toSeq

    // ElementLockProvider.removeLocksInAllModels(lock.getId())

    ConflictResolutionResponse(response, performanceInformation)
  }
}
