package de.tudresden.inf.st.rmsc.quickstart

import de.tudresden.inf.st.crme.models.MetaModels
import org.rosi_project.model_management.{ClientChangeRecord, ConflictResolution}
import org.rosi_project.model_management.sync.procedures.data.ClientEntireRecord

import scala.collection.mutable.ListBuffer


object Main {
  final val MEMBER_MODEL_TYPE = "de.tudresden.inf.st.crme.models.modelB.Member"

  def main(args: Array[String]): Unit = {
    MetaModels.initCrmMetaModels()

    val clientChangeRecords = ListBuffer[ClientChangeRecord]()
    val clientEntireRecords = ListBuffer[ClientEntireRecord]()

    // Add an example member object
    clientChangeRecords.append(ClientChangeRecord(MEMBER_MODEL_TYPE, s"e1", 0, 0,
      Map("birthday" -> s"01.01.1995", "lastName" -> s"Member 1", "address" -> "Dresden"), Map(), Map(), s"e1", s"testuser1", refonly = false))
    clientEntireRecords.append(ClientEntireRecord(s"e1", 0, 0, MEMBER_MODEL_TYPE))

    ConflictResolution.syncModel("creation", clientChangeRecords, clientEntireRecords, 1, s"testuser1")
  }
}
