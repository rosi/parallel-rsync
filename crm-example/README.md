# Resolving Conflicts in Role-based Multimodel-Synchronization (Example)

Supplementary material for the [COP 2021](https://conf.researchr.org/track/ecoop-issta-2021/ecoop-issta-2021-cop) publication *"Resolving Synchronization Conflicts in Role-based Multimodel-Synchronization Environments"* by Sebastian Ebert, Tim Kluge , Sebastian Götz, Uwe Aßmann and Christopher Werner.

## Installation

This project is based on Maven, thus building it is simple:

1. clone this project into your workspace
2. cd into the cloned project
3. run mvn -compile
4. run mvn -package

## Setting up IntelliJ

1. Install IntelliJ's Plugin for Scala-Support
2. Clone this maven project
3. Import the project into IntelliJ
4. Wait till all dependencies are downloaded / installed

### Prerequisites

* Java SE Development Kit 8 (minimum)
* Maven 3.6.0 (minimum)

### Understanding the code package by package

**de.tudresden.inf.st.crme.models**

* contains 3 packages which specify the models used in the COP 2021 publication
* a model element is composed of its fields and synchronizations methods calling the sync-roles of RSYNC (below example from Employee.scala)

```
def syncFieldsToOtherModels(): Unit = {
    +this syncEmployeeFields()
}
```

* all modelelements need to extend ConflictResolvable to be able to be integrated into parallel RSYNC
* additionally a MetaModels-Object is defined, tagging fields of the model elements, this is required for the underlying sync libary to know all fields

**de.tudresden.inf.st.crme.rsync.compartments**

* specification of compartements for the synchronization of construction, destruction and changes between models, based on RSYNC
* in the example we have 1 context for each operation, with rules specifying the model transformations
* e.g. the CrmModelsSyncCompartment specifies a Sync-Rule containing a method syncSimpleEmployeeFields, which describes what will happen if the fields of a SimpleEmployee were changed
* the dosync-flag inside eacht sync-method is needed to avoid infinite synchronization loops

**de.tudresden.inf.st.crme.rules**

* subpackage customrules: contains several reference specific rules
* subpackage systemrules: contains system rules which are either version or owner based
