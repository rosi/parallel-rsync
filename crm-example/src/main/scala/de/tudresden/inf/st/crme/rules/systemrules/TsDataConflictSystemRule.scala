package de.tudresden.inf.st.crme.rules.systemrules

import org.rosi_project.model_management.core.PlayerSync
import org.rosi_project.model_management.core.ModelElementLists
import org.rosi_project.model_management.sync.conflict.systemrules.{DataConflictSystemRule, SystemRule}
import org.rosi_project.model_management.sync.lock._
import org.slf4j.LoggerFactory

/**
 * Data-conflict-system-rule which integrates the data-version mechanism.
 */
@SystemRule("data")
class TsDataConflictSystemRule extends DataConflictSystemRule {

  val logger = LoggerFactory.getLogger(classOf[TsDataConflictSystemRule])

  def getRuleName() = "TsDataConflictSystemRule"

  def resolveModificationConflicts(ts: Int, modifier: String, guid: String, elementKey: String): (Boolean, PlayerSync) = {

    val elementSet: Set[SynchronizationAware] = ModelElementLists.getDirectElementsFromType(elementKey)

    elementSet.foreach { elem =>
      val playerSync: SynchronizationAware = elem.asInstanceOf[SynchronizationAware]

      // check for conflicts
      if (playerSync.guid == guid) {
        if (playerSync.deleted) {
          logger.error("Detected deleted element in ModelElementsList ... this should not occur!")
        }
        // check if mod-mod-conflict
        if (playerSync.ts > ts) {
          logger.error("TS-Rule detected conflict (MOD-MOD): " + guid)
          return (false, playerSync)
        } else {
          logger.info("TS-Rule passed by " + guid)
          return (true, playerSync)
        }
      }
    }
    logger.error("TS-Rule detected conflict (DEL-MOD): " + guid)
    (false, null)
  }

  def resolveDeletionConflicts(ts: Int, modifier: String, guid: String, elementKey: String): (Boolean, PlayerSync) = {

    val elementSet: Set[SynchronizationAware] = ModelElementLists.getDirectElementsFromType(elementKey)

    elementSet.foreach { elem =>
      val playerSync: SynchronizationAware = elem.asInstanceOf[SynchronizationAware]

      if (playerSync.guid == guid) {
        if (playerSync.deleted) {
          logger.error("Detected deleted element in ModelElementsList ... this should not occur!")
        }
        // check if mod-del-conflict
        if (playerSync.ts > (ts * (-1))) {
          logger.error("TS-Rule detected conflict (MOD-DEL): " + guid)
          return (false, playerSync)
        } else {
          logger.info("TS-Rule passed by " + guid)
          return (true, playerSync)
        }
      }
    }
    logger.error("TS-Rule detected conflict (DEL-DEL): " + guid)
    (false, null)
  }
}
