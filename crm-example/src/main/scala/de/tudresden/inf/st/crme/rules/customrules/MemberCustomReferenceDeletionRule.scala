package de.tudresden.inf.st.crme.rules.customrules

import de.tudresden.inf.st.crme.models.modelB.Team
import org.rosi_project.model_management.core.PlayerSync
import org.rosi_project.model_management.sync.conflict.customrules.ICustomReferenceRule
import org.slf4j.LoggerFactory
import org.rosi_project.model_management.sync.lock._


class MemberCustomReferenceDeletionRule extends ICustomReferenceRule {

  private val logger = LoggerFactory.getLogger(classOf[MemberCustomReferenceDeletionRule])

  /**
   * returns the name of this rule
   */
  def getRuleName(): String = "MemberCustomReferenceCreationRule"

  /**
   * true -> it is a ReferenceCreationRule
   */
  def isReferenceDeletionRule(): Boolean = true

  /**
   * return true if rule allows deletion of reference
   */
  def matchesRule(referencingElement: PlayerSync, referencedElement: PlayerSync, senderRv: Int, senderOwner: String, collection: String): Boolean = {
    if (referencingElement.isInstanceOf[Team]) {
      val team: Team = referencingElement.asInstanceOf[Team]
      team.getMembers().foreach { c =>
        if (c.guid == referencedElement.asInstanceOf[SynchronizationAware].guid) {
          return true
        }
      }
      // REF-DEL-DEL
      logger.error("Detected conflict (REF-DEL-DEL): " + team.guid)
      return false
    }
    false
  }
}