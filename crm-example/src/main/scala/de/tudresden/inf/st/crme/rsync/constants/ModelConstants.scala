package de.tudresden.inf.st.crme.rsync.constants

object ModelConstants {

  final val SIMPLE_EMPLOYEE_MODEL_TYPE = "de.tudresden.inf.st.crme.models.modelC.SimpleEmployee"

  final val MEMBER_MODEL_TYPE = "de.tudresden.inf.st.crme.models.modelB.Member"
  final val TEAM_MODEL_TYPE = "de.tudresden.inf.st.crme.models.modelB.Team"

  final val EMPLOYEE_MODEL_TYPE = "de.tudresden.inf.st.crme.models.modelA.Employee"
  final val PRESENCE_EMPLOYEE_MODEL_TYPE = "de.tudresden.inf.st.crme.models.modelA.PresenceEmployee"
  final val REMOTE_EMPLOYEE_MODEL_TYPE = "de.tudresden.inf.st.crme.models.modelA.RemoteEmployee"

}
