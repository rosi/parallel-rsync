package de.tudresden.inf.st.crme.models

import de.tudresden.inf.st.crme.models.modelA.{Employee, PresenceEmployee, RemoteEmployee}
import de.tudresden.inf.st.crme.models.modelB.{Member, Team}
import de.tudresden.inf.st.crme.models.modelC.SimpleEmployee
import de.tudresden.inf.st.crme.rsync.compartments.{CrmModelsConstructionCompartment, CrmModelsDestructionCompartment, CrmModelsSyncCompartment}
import de.tudresden.inf.st.crme.rules.customrules.{LeaderCustomReferenceCreationRule, MemberCustomDeletionRule, MemberCustomReferenceCreationRule, MemberCustomReferenceDeletionRule}
import de.tudresden.inf.st.crme.rules.systemrules.{OwnerDataConflictSystemRule, TsDataConflictSystemRule, TsReferenceConflictSystemRule}
import org.rosi_project.model_management.core.{Cardinality, Field, ModelElementLists, ModelInformation, SynchronizationCompartment}
import org.rosi_project.model_management.sync.conflict.customrules.CustomRuleExecutor
import org.rosi_project.model_management.sync.conflict.systemrules.SystemRuleExecutor

import scala.reflect.classTag

object MetaModels {
  def reset(): Unit = {
    ModelElementLists.dropAll()
    initCrmMetaModels()
    System.gc()
  }

  def initCrmMetaModels(): Unit ={
    // Model A
    ModelInformation.add(
      ModelInformation(classTag[PresenceEmployee].runtimeClass, classTag[Employee].runtimeClass),
      ModelInformation(classTag[Employee].runtimeClass, null, Field.primitives(classTag[String], "fullName", "birthday", "address") ++
        Field.primitives(classTag[Float], "salary")),
      ModelInformation(classTag[RemoteEmployee].runtimeClass, classTag[Employee].runtimeClass))

    // Model B
    ModelInformation.add(
      ModelInformation(classTag[Team].runtimeClass, null, Field.primitives(classTag[String], "teamName") ++
        Set(Field.association(classTag[Member], "members", Cardinality.Many,
          (team, member) => team.asInstanceOf[Team].addMember(member.asInstanceOf[Member]),
          (team, member) => team.asInstanceOf[Team].removeMember(member.asInstanceOf[Member])),

          Field.association(classTag[Member], "leader", Cardinality.One,
            (team, leader) => team.asInstanceOf[Team].setLeader(leader.asInstanceOf[Member]),
            (team, leader) => team.asInstanceOf[Team].setLeader(null))
        )),
      ModelInformation(classTag[Member].runtimeClass, null, Field.primitives(classTag[String], "birthday", "firstName", "companyLocation", "lastName", "address") ++
        Set(Field.association(classTag[Team], "leaderInTeam", Cardinality.One,
          (member, team) => member.asInstanceOf[Member].setLeaderInTeam(team.asInstanceOf[Team]),
          (member, team) => member.asInstanceOf[Member].setLeaderInTeam(null)),

          Field.association(classTag[Member], "memberInTeam", Cardinality.One,
            (member, team) => member.asInstanceOf[Member].setMemberInTeam(team.asInstanceOf[Team]),
            (member, team) => member.asInstanceOf[Member].setMemberInTeam(null)),
        )))

    // Model C
    ModelInformation.add(ModelInformation(classTag[SimpleEmployee].runtimeClass, null, Field.primitives(classTag[String], "birthday", "team", "fullName", "address") ++
      Field.primitives(classTag[Boolean], "isRemoteEmployee")))

    // RSync Setup
    SynchronizationCompartment.reset()
    SynchronizationCompartment.createRoleManager()
    SynchronizationCompartment.changeConstructionRule(CrmModelsConstructionCompartment)
    SynchronizationCompartment.changeDestructionRule(CrmModelsDestructionCompartment)
    SynchronizationCompartment.addSynchronizationRule(new CrmModelsSyncCompartment())

    SystemRuleExecutor.addSystemDataRule(new TsDataConflictSystemRule, new OwnerDataConflictSystemRule)
    SystemRuleExecutor.addSystemReferenceRule(new TsReferenceConflictSystemRule)

    CustomRuleExecutor.addCustomDeletionRule(new MemberCustomDeletionRule)
    CustomRuleExecutor.addCustomReferenceRule(new MemberCustomReferenceCreationRule, new MemberCustomReferenceDeletionRule, new LeaderCustomReferenceCreationRule)
  }
}
