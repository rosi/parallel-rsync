package de.tudresden.inf.st.crme.models.modelC

import de.tudresden.inf.st.crme.models.modelA.Employee
import org.rosi_project.model_management.core.{ConflictResolvable, Field, ModelInformation, PlayerSync}
import org.rosi_project.model_management.sync.lock.{ElementLock, SynchronizationAware}


class SimpleEmployee  (protected var birthday: String, protected  var isRemoteEmployee : Boolean, var team: String,
                       protected var fullName: String, protected var address: String, lock: ElementLock,
                       guid: String, owner: String) extends ConflictResolvable (lock, guid, owner){

  def this(guid: String, lock: ElementLock, data: Map[String, String]) = this(data("birthday"),
    data("isRemoteEmployee").toBoolean, data("team"), data("fullName"), data("address"), lock, guid, data("owner"))

  override def serialize(): Map[String, String] = Map("isRemoteEmployee" -> isRemoteEmployee.toString, "fullName" -> fullName,
    "birthday" -> birthday, "address" -> address, "team" -> team) ++ super.serialize()

  override def unserialize(data: Map[String, String]): Unit = {
    super.unserialize(data)
    isRemoteEmployee = data("isRemoteEmployee").toBoolean
    fullName = data("fullName")
    birthday = data("birthday")
    address = data("address")
    team = data("team")
  }

  def syncFieldsToOtherModels(): Unit = {
    +this syncSimpleEmployeeFields()
  }

  def getIsRemoteEmployee(): Boolean = {
    isRemoteEmployee
  }

  def setIsRemoteEmployee(b : Boolean): Unit = {
    isRemoteEmployee = b
  }

  def getTeam(): String = {
    team
  }

  def setTeam(s : String): Unit = {
    team = s
  }

  def getFullName(): String = {
    fullName
  }

  def setFullName(s : String): Unit = {
    fullName = s
  }

  def getAddress(): String = {
    address
  }

  def setAddress(s : String): Unit = {
    address = s
  }

  def getBirthday(): String = {
    birthday
  }

  def setBirthday(s : String): Unit = {
    birthday = s
  }

  def getRelated: Set[PlayerSync] = {
    val relatedElementsMetaObject = +this getRelatedElements()
    val relatedObjects: Set[PlayerSync] = relatedElementsMetaObject.right.get.head.right.get
    relatedObjects
  }

  override def toString: String = {

    var lockData: String = ""

    if(this.lock != null){
      lockData += this.lock.getId()
    }else{
      lockData += "---"
    }

    return "SimpleEmployee: " + guid + " ts: "  + ts + " rv: " + rv + " owner: " + owner  +
      " isRemoteEmployee: " + isRemoteEmployee + " fullName: " + fullName + " birthday: " +
      birthday  +  " address: " + address + " team: " + team + " D: " + deleted + " lock: " + lockData
  }
}