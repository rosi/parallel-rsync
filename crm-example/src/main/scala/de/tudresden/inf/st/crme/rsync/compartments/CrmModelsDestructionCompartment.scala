package de.tudresden.inf.st.crme.rsync.compartments

import org.rosi_project.model_management.core.{ModelElementLists, PlayerSync}
import org.rosi_project.model_management.sync.IDestructionCompartment
import org.rosi_project.model_management.sync.roles.{IDestructor, IRoleManager}
import org.slf4j.LoggerFactory

object CrmModelsDestructionCompartment extends IDestructionCompartment{

  override def getRuleName: String = "CrmModelsDestruction"

  override def getDestructorForClassName(classname: Object): IDestructor = {
    new DeleteRoleAndConnections();
  }

  /**
   * Destruction-role that deletes objects in all models
   */
  class DeleteRoleAndConnections() extends IDestructor {

    val logger = LoggerFactory.getLogger(classOf[DeleteRoleAndConnections])

    def deleteRoleFunction(): Unit = {

      logger.info("%%%%%%% Executing DeleteRoleAndConnections %%%%%%%")
      //get the list of related manager
      var relatedManagers: Set[IRoleManager] = (+this).getRelatedManager()

      //clear all lists from the related managers
      (+this).clearListsOfRelatedManager()

      //delete also all related elements
      relatedManagers.foreach { m =>
        (+m).deleteObjectFromSynchro()
      }

      //send notification about deletion
      (+this).deletionNotification()

      //clear now the related manager list
      (+this).clearRelatedManager()
      //delete all roles this element has
      val player = this.player
      if (player.isRight) {
        val test: PlayerSync = player.right.get.asInstanceOf[PlayerSync]
        ModelElementLists.removeElement(test)
        val roles = test.roles()
        roles.foreach { r =>
          r.remove()
        }
      }
    }
  }
}
