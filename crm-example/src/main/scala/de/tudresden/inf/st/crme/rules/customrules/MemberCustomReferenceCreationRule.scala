package de.tudresden.inf.st.crme.rules.customrules

import de.tudresden.inf.st.crme.models.modelB.Team
import org.rosi_project.model_management.core.PlayerSync
import org.rosi_project.model_management.sync.conflict.customrules.ICustomReferenceRule
import org.rosi_project.model_management.sync.lock.SynchronizationAware
import org.slf4j.LoggerFactory

class MemberCustomReferenceCreationRule extends ICustomReferenceRule{

  private val log = LoggerFactory.getLogger(classOf[MemberCustomReferenceCreationRule])

  override def getRuleName(): String = "MemberCustomReferenceCreationRule"

  override def isReferenceDeletionRule(): Boolean = false

  override def matchesRule(referencingElement: PlayerSync, referencedElement: PlayerSync, senderRv: Int, senderOwner: String, collection: String): Boolean = {

    if (referencingElement.isInstanceOf[Team]) {
      val team: Team = referencingElement.asInstanceOf[Team]
      team.getMembers().foreach { c =>
        if (c.guid == referencedElement.asInstanceOf[SynchronizationAware].guid) {
          // REF-ADD-ADD
          log.info("Detected conflict (REF-ADD-ADD): " + team.guid)
          return false
        }
      }
      return true
    }
    false
  }
}
