package de.tudresden.inf.st.crme.rsync.compartments

import de.tudresden.inf.st.crme.models.modelA.{Employee, PresenceEmployee, RemoteEmployee}
import de.tudresden.inf.st.crme.models.modelB.{Member, Team}
import de.tudresden.inf.st.crme.models.modelC.SimpleEmployee
import org.rosi_project.model_management.core.{ModelElementLists, PlayerSync, SynchronizationCompartment}
import org.rosi_project.model_management.sync.IConstructionCompartment
import org.rosi_project.model_management.sync.lock.{ElementLock, SynchronizationAware}
import org.rosi_project.model_management.sync.roles.{IConstructor, IRoleManager}
import org.rosi_project.model_management.util.UuidUtil

object CrmModelsConstructionCompartment extends IConstructionCompartment {

  override def getRuleName: String = "CrmModelsConstruction"

  override def getConstructorForClassName(classname: Object): IConstructor = {

    if (classname.isInstanceOf[Member]) {
      return new MemberConstruct()
    }

    if (classname.isInstanceOf[Team]) {
      return new TeamConstruct()
    }

    if (classname.isInstanceOf[SimpleEmployee]) {
      return new SimpleEmployeeConstruct()
    }

    if(classname.isInstanceOf[Employee] || classname.isInstanceOf[PresenceEmployee] || classname.isInstanceOf[RemoteEmployee]){
      return new EmployeeConstruct()
    }

    null
  }

  class EmployeeConstruct() extends IConstructor {

    override def construct(comp: PlayerSync, man: IRoleManager): Unit = {

      val birthday: String = +this getBirthday()
      val fullName: String = +this getFullName()
      val address: String = +this getAddress()
      val owner: String = +this getOwner()
      val splitName: Array[String] = fullName.split(" ")
      var firstName: String = null
      var lastName: String = null

      if (splitName.size == 2) {
        firstName = splitName(0);
        lastName = splitName(1);
      }

      val lock: ElementLock = comp.asInstanceOf[SynchronizationAware].lock

      val simpleEmployee: SimpleEmployee = new SimpleEmployee(birthday, comp.isInstanceOf[RemoteEmployee], null,
        fullName, address, lock, UuidUtil.generateUuid(), owner)
      simpleEmployee.ts = 1
      simpleEmployee.rv = 1

      val member: Member = new Member(birthday, firstName, "DEFAULT", lastName, address
        , null, null, lock, UuidUtil.generateUuid(), owner)
      member.ts = 1
      member.rv = 1

      createContainerElement(true, true, comp, man)
      createContainerElement(false, true, simpleEmployee, SynchronizationCompartment.createRoleManager())
      createContainerElement(false, true, member, SynchronizationCompartment.createRoleManager())

      makeCompleteConstructionProcess(containers)
    }
  }

  class MemberConstruct() extends IConstructor {

    override def construct(comp: PlayerSync, man: IRoleManager): Unit = {

      val birthday: String = +this getBirthday()
      val firstName: String = +this getFirstName()
      val companyLocation: String = +this getCompanyLocation()
      val lastName: String = +this getLastName()
      val address: String = +this getAddress()
      val memberInTeam: Team = +this getMemberInTeam()
      val owner: String = +this getOwner()

      val isRemoteWorker = (companyLocation == null)
      val fullName = firstName + " " + lastName

      val lock: ElementLock = comp.asInstanceOf[SynchronizationAware].lock

      var tName: String = null

      if(memberInTeam != null){
        tName = memberInTeam.getTeamName()
      }

      val simpleEmployee: SimpleEmployee = new SimpleEmployee(birthday, isRemoteWorker, tName,
        fullName, address, lock, UuidUtil.generateUuid(), owner)
      simpleEmployee.ts = 1
      simpleEmployee.rv = 1

      var employee: Employee = null
      var teamName: String = null

      if(memberInTeam != null){
        teamName = memberInTeam.getTeamName()
      }

      if(companyLocation == null){
        employee = new RemoteEmployee(fullName, birthday, address, -1f, lock, UuidUtil.generateUuid(), owner)
      }else{
        employee = new PresenceEmployee(fullName, birthday, address, -1f, lock, UuidUtil.generateUuid(), owner)
      }

      employee.ts = 1
      employee.rv = 1

      createContainerElement(true, true, comp, man)
      createContainerElement(false, true, simpleEmployee, SynchronizationCompartment.createRoleManager())
      createContainerElement(false, true, employee, SynchronizationCompartment.createRoleManager())

      makeCompleteConstructionProcess(containers)
    }
  }

  class TeamConstruct() extends IConstructor {

    override def construct(comp: PlayerSync, man: IRoleManager): Unit = {

      comp.ts = 1
      comp.rv = 1

      createContainerElement(true, true, comp, man)
      makeCompleteConstructionProcess(containers)

    }
  }

  class SimpleEmployeeConstruct() extends IConstructor {

    override def construct(comp: PlayerSync, man: IRoleManager): Unit = {

      val birthday: String = +this getBirthday()
      val isRemoteEmployee: Boolean = +this getIsRemoteEmployee()
      val fullName: String = +this getFullName()
      val address: String = +this getAddress()
      val owner: String = +this getOwner()
      val teamName: String = +this getTeam()

      val lock: ElementLock = comp.asInstanceOf[SynchronizationAware].lock
      val splitName: Array[String] = fullName.split(" ")
      var firstName: String = null
      var lastName: String = null
      var companyLocation: String = null

      if (splitName.size == 2) {
        firstName = splitName(0);
        lastName = splitName(1);
      }

      if(!isRemoteEmployee){
        companyLocation = "DEFAULT"
      }

/*      val team: Team = new Team(teamName, null, null, lock, UuidUtil.generateUuid(), owner)
      team.ts = 1
      team.rv = 1 */

      val member: Member = new Member(birthday, firstName, companyLocation, lastName, address
        , null, null, lock, UuidUtil.generateUuid(), owner)
      member.ts = 1
      member.rv = 1

//      team.addMemberWithoutSync(member)

      var employee: Employee = null

      if(companyLocation == null){
        employee = new RemoteEmployee(fullName, birthday, address, -1f, lock, UuidUtil.generateUuid(), owner)
      }else{
        employee = new PresenceEmployee(fullName, birthday, address, -1f, lock, UuidUtil.generateUuid(), owner)
      }

      employee.ts = 1
      employee.rv = 1

      createContainerElement(true, true, comp, man)
      createContainerElement(false, true, member, SynchronizationCompartment.createRoleManager())
      // createContainerElement(false, true, team, SynchronizationCompartment.createRoleManager())
      createContainerElement(false, true, employee, SynchronizationCompartment.createRoleManager())

      makeCompleteConstructionProcess(containers)
    }
  }
}
