package de.tudresden.inf.st.crme

import de.tudresden.inf.st.crme.models.modelA.PresenceEmployee
import de.tudresden.inf.st.crme.models.modelB.{Member, Team}
import de.tudresden.inf.st.crme.models.modelC.SimpleEmployee
import de.tudresden.inf.st.crme.rsync.compartments.{CrmModelsConstructionCompartment, CrmModelsDestructionCompartment, CrmModelsSyncCompartment}
import de.tudresden.inf.st.crme.rsync.constants.ModelConstants
import org.rosi_project.model_management.core.{ModelElementLists, PlayerSync, SynchronizationCompartment}
import org.rosi_project.model_management.sync.lock.{ElementLock, ElementLockProvider, SynchronizationAware}
import org.rosi_project.model_management.util.UuidUtil
import org.slf4j.LoggerFactory
import scroll.internal.Compartment

object Application extends App {


}
