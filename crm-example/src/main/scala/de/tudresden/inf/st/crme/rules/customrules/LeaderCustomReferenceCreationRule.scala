package de.tudresden.inf.st.crme.rules.customrules

import de.tudresden.inf.st.crme.models.modelA.Employee
import de.tudresden.inf.st.crme.models.modelB.Team
import org.rosi_project.model_management.core.PlayerSync
import org.rosi_project.model_management.sync.conflict.customrules.ICustomReferenceRule
import org.slf4j.LoggerFactory
import scroll.internal.Compartment

class LeaderCustomReferenceCreationRule extends ICustomReferenceRule {

  override def getRuleName(): String = "LeaderCustomReferenceCreationRule"

  override def isReferenceDeletionRule(): Boolean = false

  override def matchesRule(referencingElement: PlayerSync, referencedElement: PlayerSync, senderRv: Int, senderOwner: String, collection: String): Boolean = {
    var isEligible = false

    new Compartment {
      referencingElement match {
        case _: Team => {
          if (collection == "leader") {
            val related: Set[AnyRef] = (+referencingElement).getRelatedElements()
            related.foreach {
              case s: Employee => isEligible  = s.getSalary() >= 3000
            }
          }
        }
      }
    }

    return isEligible
  }
}
