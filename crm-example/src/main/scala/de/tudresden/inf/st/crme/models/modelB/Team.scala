package de.tudresden.inf.st.crme.models.modelB

import de.tudresden.inf.st.crme.models.modelA.Employee
import org.rosi_project.model_management.core.{AssociationField, Cardinality, ConflictResolvable, Field, ModelInformation, PlayerSync}
import org.rosi_project.model_management.sync.lock.{ElementLock, SynchronizationAware}

import scala.::
import scala.reflect.classTag

class Team(protected var teamName: String, protected var leader: Member,
           protected var members: Set[Member],
           tLock: ElementLock, tGuid: String, tOwner: String) extends ConflictResolvable (tLock, tGuid, tOwner){

  def this(guid: String, lock: ElementLock, data: Map[String, String]) = this(data("teamName"),
    ModelInformation.fromGuid(data("leader")).asInstanceOf[Member], ModelInformation.fromGuids(classTag[Member], data("members")).map(x => x.asInstanceOf[Member]), lock, guid, data("owner"))

  override def serialize(): Map[String, String] = Map("teamName" -> teamName, "members" -> ModelInformation.getGuids(members.toSet[SynchronizationAware]),
    "leader" -> ModelInformation.getGuid(leader)) ++ super.serialize()

  override def unserialize(data: Map[String, String]): Unit = {
    super.unserialize(data)
    if(data.contains("teamName")) teamName = data("teamName")
    if(data.contains("members")) members = ModelInformation.fromGuids(classTag[Member], data("members")).map(x => x.asInstanceOf[Member])
    if(data.contains("leader")) leader = ModelInformation.fromGuid(data("leader")).asInstanceOf[Member]
  }

  def syncFieldsToOtherModels(): Unit = {
    +this syncTeamFields()
  }

  def setLeader(m: Member): Unit = {
    leader = m
  }

  def getLeader(): Member = {
    leader
  }

  def setTeamName(s: String): Unit = {
    teamName = s
  }

  def getTeamName(): String = {
    teamName
  }

  def getMembers(): Set[Member] = {
    members
  }

  def addMember(m: Member): Unit = {
    addMemberWithoutSync(m)
    +this syncAddMember (m)
  }

  def addMemberWithoutSync(m: Member): Unit = {

    if(members == null){
      members = Set()
    }

    require(m != null)
    require(!members.contains(m))
    members += m
  }

  def removeMember(m: Member): Unit = {
    removeMemberWithoutSync(m)
    +this syncRemoveMember (m)
  }

  def addMemberOnlySync(m: Member): Unit = +this syncAddMember(m)

  def removeMemberOnlySync(m: Member): Unit = +this syncRemoveMember(m)

  def removeMemberWithoutSync(m: Member): Unit = {
    require(m != null)
    require(members.contains(m))
    members -= m
  }

  def getRelated: Set[PlayerSync] = {
    val relatedElementsMetaObject = +this getRelatedElements()
    val relatedObjects: Set[PlayerSync] = relatedElementsMetaObject.right.get.head.right.get
    relatedObjects
  }

  override def toString: String = {

    var mString: String = ""

    getMembers().foreach { m =>
      mString += " ++ Member: "
      mString += m.getLastName
    }

    var lockData: String = ""

    if(this.lock != null){
      lockData += this.lock.getId()
    }else{
      lockData += "---"
    }

    return "Team: " + guid + " ts: "  + ts + " rv: " + rv + " owner: " + tOwner + " teamname: " + teamName + " leader: "+
      leader + " D: " + deleted + " lock: " + lockData + " ** With members:" + mString
  }

}