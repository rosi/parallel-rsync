package de.tudresden.inf.st.crme.models.modelB

import org.rosi_project.model_management.core.{ConflictResolvable, ModelInformation, PlayerSync}
import org.rosi_project.model_management.sync.lock.{ElementLock, SynchronizationAware}

class Member (protected var birthday: String, protected var firstName: String, protected  var companyLocation : String,
              protected var lastName: String, protected var address: String,
              protected var leaderInTeam: Team, protected var memberInTeam: Team,
              lock: ElementLock, guid: String, owner: String) extends ConflictResolvable (lock, guid, owner){

  def this(guid: String, lock: ElementLock, map: Map[String, String]) = this(map("birthday"), map("fullname"), map("companyLocation"), map("lastName"),
    map("address"), ModelInformation.fromGuid(map("leaderInTeam")).asInstanceOf[Team], ModelInformation.fromGuid(map("memberInTeam")).asInstanceOf[Team], lock, guid, map("owner"))

  override def serialize: Map[String, String] = Map("birthday" -> birthday, "firstName" -> firstName, "lastName" -> lastName,
    "companyLocation" -> companyLocation, "address" -> address, "leaderInTeam" -> ModelInformation.getGuid(leaderInTeam),
    "memberInTeam" -> ModelInformation.getGuid(memberInTeam)) ++ super.serialize()

  override def unserialize(data: Map[String, String]): Unit = {
    super.unserialize(data)
    birthday = data("birthday")
    firstName = data("firstName")
    lastName = data("lastName")
    companyLocation = data("companyLocation")
    address = data("address")
    leaderInTeam = ModelInformation.fromGuid(data("leaderInTeam")).asInstanceOf[Team]
    memberInTeam = ModelInformation.fromGuid(data("memberInTeam")).asInstanceOf[Team]
  }

  def syncFieldsToOtherModels(): Unit = {
    +this syncMemberFields()
  }

  def getCompanyLocation(): String = {
    companyLocation
  }

  def getFullName() = s"${firstName} ${lastName}"

  def setCompanyLocation(s : String): Unit = {
    companyLocation = s
  }

  def getLastName(): String = {
    lastName
  }

  def setLastName(s : String): Unit = {
    lastName = s
  }

  def getFirstName(): String = {
    firstName
  }

  def setFirstName(s : String): Unit = {
    firstName = s
  }

  def getAddress(): String = {
    address
  }

  def setAddress(s : String): Unit = {
    address = s
  }

  def getBirthday(): String = {
    birthday
  }

  def setBirthday(s : String): Unit = {
    birthday = s
  }

  def getLeaderInTeam(): Team = {
    leaderInTeam
  }

  def setLeaderInTeam(t : Team): Unit = {
    leaderInTeam = t
    +this syncLeaderInTeam()
  }

  def getMemberInTeam(): Team = {
    memberInTeam
  }

  def setMemberInTeam(t : Team): Unit = {
    memberInTeam = t
    +this syncMemberInTeam()
  }

  def setMemberWithoutSyncInTeam(t : Team): Unit = {
    memberInTeam = t
  }

  def getRelated: Set[PlayerSync] = {
    val relatedElementsMetaObject = +this getRelatedElements()
    val relatedObjects: Set[PlayerSync] = relatedElementsMetaObject.right.get.head.right.get
    relatedObjects
  }

  override def toString: String = {

    var lockData: String = ""
    var teamName = "---"

    if(memberInTeam != null){
      teamName = memberInTeam.getTeamName()
    }

    if(this.lock != null){
      lockData += this.lock.getId()
    }else{
      lockData += "---"
    }

    return "Member: " + guid + " ts: "  + ts + " rv: " + rv + " owner: " + owner  + " memberteam: "+
      teamName + " firstname: " + firstName + " lastName: " + lastName + " birthday: " +
      birthday  + " companyLocation: " + companyLocation  + " address: " + address + " D: " + deleted + " lock: " + lockData
  }
}