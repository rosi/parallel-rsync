package de.tudresden.inf.st.crme.models.modelA

import org.rosi_project.model_management.core.ModelInformation
import org.rosi_project.model_management.sync.lock.ElementLock

import scala.reflect.classTag

class RemoteEmployee (fullName: String, birthday: String,
                      address: String, salary: Float, lock: ElementLock,
                      guid: String, owner: String)
  extends Employee (fullName, birthday, address, salary, lock, guid, owner) {

  def this(guid: String, lock: ElementLock, data: Map[String, String]) = this(data("fullName"), data("birthday"),
    data("address"), data("salary").toFloat, lock, guid, data("owner"))

  override def toString(): String = {

    var lockData: String = ""

    if(this.lock != null){
      lockData += this.lock.getId()
    }else{
      lockData += "---"
    }

    return "RemoteEmployee: " + getFullName() + " rbirthday: " + getBirthday() + " raddress: " +
      getAddress()  + " rSalary: " + getSalary()  + " reGuid: " + guid + " D: " + deleted + " lock: " + lockData
  }
}
