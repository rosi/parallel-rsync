package de.tudresden.inf.st.crme.models.modelA

import org.rosi_project.model_management.core
import org.rosi_project.model_management.core.{ConflictResolvable, Field, MapSerializable, ModelInformation, PlayerSync, PrimitiveField}
import org.rosi_project.model_management.sync.lock.{ElementLock, SynchronizationAware}

import scala.reflect.classTag

abstract class Employee (protected var fullName: String, protected var birthday: String,
                protected var address: String, protected var salary: Float,
                         lock: ElementLock, guid: String, owner: String) extends ConflictResolvable(lock, guid, owner) {

  def this(guid: String, lock: ElementLock, data: Map[String, String]) = this(data("fullName"), data("birthday"),
    data("address"), data("salary").toFloat, lock, guid, data("owner"))

  override def serialize: Map[String, String] = Map("fullName" -> fullName, "birthday" -> birthday, "adress" -> address, "salary" -> salary.toString) ++ super.serialize()

  override def unserialize(data: Map[String, String]): Unit ={
    super.unserialize(data)
    if(data.contains("fullName")) fullName = data("fullName")
    if(data.contains("birthday")) birthday = data("birthday")
    if(data.contains("address")) address = data("address")
    if(data.contains("salary")) salary = data("salary").toFloat
  }

  def syncFieldsToOtherModels(): Unit = {
    +this syncEmployeeFields()
  }

  def getFullName(): String = {
    fullName
  }

  def setFullName(s : String): Unit = {
    fullName = s
  }

  def getSalary(): Float = {
    salary
  }

  def setSalary(f : Float): Unit = {
    salary = f
  }

  def getAddress(): String = {
    address
  }

  def setAddress(s : String): Unit = {
    address = s
  }

  def getBirthday(): String = {
    birthday
  }

  def setBirthday(s : String): Unit = {
    birthday = s
  }

  def getRelated: Set[PlayerSync] = {
    val relatedElementsMetaObject = +this getRelatedElements()
    val relatedObjects: Set[PlayerSync] = relatedElementsMetaObject.right.get.head.right.get
    relatedObjects
  }

  override def toString(): String = {
    return "Employee: " + fullName + " D: " + deleted
  }
}
