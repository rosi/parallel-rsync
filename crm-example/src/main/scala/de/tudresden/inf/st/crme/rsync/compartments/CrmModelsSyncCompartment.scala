package de.tudresden.inf.st.crme.rsync.compartments

import de.tudresden.inf.st.crme.models.modelA.Employee
import de.tudresden.inf.st.crme.models.modelB.{Member, Team}
import de.tudresden.inf.st.crme.models.modelC.SimpleEmployee
import de.tudresden.inf.st.crme.rsync.constants.ModelConstants
import org.rosi_project.model_management.core.{ModelElementLists, PlayerSync}
import org.rosi_project.model_management.sync.ISyncCompartment
import org.rosi_project.model_management.sync.lock.{ElementLock, ElementLockProvider, LockResult}
import org.rosi_project.model_management.sync.roles.ISyncRole
import org.rosi_project.model_management.util.UuidUtil
import org.slf4j.LoggerFactory

import scala.sys.error

class CrmModelsSyncCompartment extends ISyncCompartment {

  override def getRuleName: String = "CrmModelsSync"

  val logger = LoggerFactory.getLogger(classOf[CrmModelsSyncCompartment])

  override protected def getFirstRole(classname: Object): ISyncRole = {
    if (classname.isInstanceOf[Employee] || classname.isInstanceOf[Member] || classname.isInstanceOf[Team]
      || classname.isInstanceOf[SimpleEmployee])
      return new Sync()
    null
  }

  override def isFirstIntegration(classname: Object): Boolean = {
    (classname.isInstanceOf[Employee] || classname.isInstanceOf[Member] || classname.isInstanceOf[Team]
      || classname.isInstanceOf[SimpleEmployee])
  }

  override def getNewInstance: ISyncCompartment = new CrmModelsSyncCompartment

  class Sync() extends ISyncRole {

    override def getOuterCompartment: ISyncCompartment = CrmModelsSyncCompartment.this

    def getRelatedElements(): Set[PlayerSync] = {
      var relatedObjects: Set[PlayerSync] = Set()
      getSyncer().foreach { a =>
        if (a.player.right.get.isInstanceOf[PlayerSync]) {
          relatedObjects += a.player.right.get.asInstanceOf[PlayerSync]
        }
      }
      relatedObjects
    }

    def syncEmployeeFields(): Unit = {
      ElementLockProvider.synchronized {
        if (!doSync) {
          // start sync
          doSync = true

          val birthday: String = +this getBirthday()
          val fullName: String = +this getFullName()
          val address: String = +this getAddress()
          val owner: String = +this getOwner()
          val splitName: Array[String] = fullName.split(" ")
          var firstName: String = null
          var lastName: String = null

          if (splitName.size == 2) {
            firstName = splitName(0);
            lastName = splitName(1);
          }

          getSyncer().foreach { syncer =>

            val ts: Int = (+syncer).ts

            if (syncer.player.right.get.isInstanceOf[SimpleEmployee]) {
              (+syncer).setFullName(fullName)
              (+syncer).setBirthday(birthday)
              (+syncer).setAddress(address)

              if (ts == Int.MaxValue) {
                (+syncer).ts = 1
              } else {
                (+syncer).ts = ts + 1
              }
            }

            if (syncer.player.right.get.isInstanceOf[Member]) {
              (+syncer).setFirstName(firstName)
              (+syncer).setLastName(lastName)
              (+syncer).setBirthday(birthday)
              (+syncer).setAddress(address)
              (+syncer).setOwner(owner)

              if (ts == Int.MaxValue) {
                (+syncer).ts = 1
              } else {
                (+syncer).ts = ts + 1
              }
            }
          }
        }
        doSync = false
      }
    }

    def syncSimpleEmployeeFields(): Unit = {
      ElementLockProvider.synchronized {
        if (!doSync) {
          // start sync
          doSync = true

          val birthday: String = +this getBirthday()
          val fullName: String = +this getFullName()
          val address: String = +this getAddress()
          val owner: String = +this getOwner()
          val isRemoteEmployee: Boolean = +this getIsRemoteEmployee()
          val teamName: String = +this getTeam()
          val splitName: Array[String] = fullName.split(" ")
          var firstName: String = null
          var lastName: String = null

          if (splitName.size == 2) {
            firstName = splitName(0);
            lastName = splitName(1);
          }

          getSyncer().foreach { syncer =>

            val ts: Int = (+syncer).ts
            val guid: String = (+syncer).guid

            if (syncer.player.right.get.isInstanceOf[Employee]) {
              (+syncer).setFullName(fullName)
              (+syncer).setOwner(owner)
              (+syncer).setAddress(address)
              (+syncer).setBirthday(birthday)

              if (ts == Int.MaxValue) {
                (+syncer).ts = 1
              } else {
                (+syncer).ts = ts + 1
              }
            }

            if (syncer.player.right.get.isInstanceOf[Member]) {
              (+syncer).setFirstName(firstName)
              (+syncer).setLastName(lastName)
              (+syncer).setBirthday(birthday)
              (+syncer).setAddress(address)
              (+syncer).setOwner(owner)
              val currentLocation: String = (+syncer).getCompanyLocation()

              if (!isRemoteEmployee && currentLocation != null) {
                (+syncer).setCompanyLocation("DEFAULT")
              }

/*              val t: Team = (+syncer).getMemberInTeam()

              var found: Boolean = false
              if (((t != null && t.getTeamName() != teamName) || t == null) && teamName != null) {
                ModelElementLists.getDirectElementsFromType(ModelConstants.TEAM_MODEL_TYPE).foreach { i =>
                  if (i.asInstanceOf[Team].getTeamName() == teamName) {
                    found = true

                    var lockSuccess: Boolean = false
                    var lockRes: LockResult = null

                    while (!lockSuccess){
                      val lockResIter = ElementLockProvider.provideLock(i.asInstanceOf[Team].guid)
                      lockRes = lockResIter
                      lockSuccess = lockResIter.success
                    }

                    (+syncer).setMemberWithoutSyncInTeam(i)
                    // enables bi-directional sync ..
                    i.asInstanceOf[Team].addMemberWithoutSync(ModelElementLists.getElementByGuid(guid).asInstanceOf[Member])
                    i.asInstanceOf[Team].rv = i.asInstanceOf[Team].rv + 1

                    ElementLockProvider.removeLocksFromElement(lockRes.lock.getId(), i.asInstanceOf[Team].guid)
                  } else if(i.asInstanceOf[Team].getMembers().exists(x => x.guid == guid)){
                    // Remove member from old team
                    val member = ModelElementLists.getElementByGuid(guid).asInstanceOf[Member]
                    i.asInstanceOf[Team].removeMemberWithoutSync(member)
                    // Remove team if now empty
                    // if(i.asInstanceOf[Team].getMembers().isEmpty) i.asInstanceOf[Team].deleteObjectFromSynchro()
                  }
                }
              } */

              if (ts == Int.MaxValue) {
                (+syncer).ts = 1
              } else {
                (+syncer).ts = ts + 1
              }
            }
          }
        }
        doSync = false
      }
    }

    def syncMemberFields(): Unit = {
      ElementLockProvider.synchronized {
        if (!doSync) {
          // start sync
          doSync = true

          val birthday: String = +this getBirthday()
          val firstName: String = +this firstName()
          val lastName: String = +this lastName()
          val address: String = +this getAddress()
          val owner: String = +this getOwner()
          val companyLocation: String = +this getCompanyLocation()

          getSyncer().foreach { syncer =>

            val ts: Int = (+syncer).ts

            if (syncer.player.right.get.isInstanceOf[Employee]) {
              (+syncer).setFullName(firstName + " " + lastName)
              (+syncer).setOwner(owner)
              (+syncer).setAddress(address)
              (+syncer).setBirthday(birthday)

              if (ts == Int.MaxValue) {
                (+syncer).ts = 1
              } else {
                (+syncer).ts = ts + 1
              }
            }

            if (syncer.player.right.get.isInstanceOf[SimpleEmployee]) {
              (+syncer).setFullName(firstName + " " + lastName)
              (+syncer).setBirthday(birthday)
              (+syncer).setAddress(address)

              if (companyLocation == null) {
                (+syncer).setIsRemoteEmployee(true)
              } else {
                (+syncer).setIsRemoteEmployee(false)
              }

              if (ts == Int.MaxValue) {
                (+syncer).ts = 1
              } else {
                (+syncer).ts = ts + 1
              }
            }
          }
        }
        doSync = false
      }
    }

    def syncTeamFields(): Unit = {
      ElementLockProvider.synchronized {
        if (!doSync) {
          // start sync
          doSync = true
          // nothing to do
        }
        doSync = false
      }
    }

    def syncLeaderInTeam(): Unit = {
      ElementLockProvider.synchronized {
        if (!doSync) {
          // start sync
          doSync = true
          // nothing to do
        }
        doSync = false
      }
    }

    def syncMemberInTeam(): Unit = {
      ElementLockProvider.synchronized {
        if (!doSync) {
          // start sync
          doSync = true
          var memberInTeam: Team = (+this).getMemberInTeam()

          getSyncer().foreach { syncer =>

            val ts: Int = (+syncer).ts

            if (syncer.player.right.get.isInstanceOf[SimpleEmployee]) {

              if (memberInTeam != null) {
                (+syncer).setTeam(memberInTeam.getTeamName())
              } else {
                (+syncer).setTeam(null)
              }

              if (ts == Int.MaxValue) {
                (+syncer).ts = 1
              } else {
                (+syncer).ts = ts + 1
              }
            }
          }
          doSync = false
        }
      }
    }

    def syncAddMember(member: PlayerSync): Unit = {
      ElementLockProvider.synchronized {
        if (!doSync) {

          // start sync
          doSync = true

          val name: String = +this getTeamName()

          member.getRelatedObjects().foreach {
            case employee: SimpleEmployee =>
              employee.setTeam(name)
            case _ =>
          }
        }
        doSync = false
      }
    }
  }

  def syncRemoveMember(member: PlayerSync): Unit = {
    ElementLockProvider.synchronized {
      if (!doSync) {

        // start sync
        doSync = true

        member.getRelatedObjects().foreach {
          case employee: SimpleEmployee =>
            employee.setTeam(null)
          case _ =>
        }
      }
      doSync = false
    }
  }
}