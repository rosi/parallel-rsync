package de.tudresden.inf.st.crme.rules.customrules

import de.tudresden.inf.st.crme.models.modelB.{Member, Team}
import org.rosi_project.model_management.core.{ModelElementLists, PlayerSync}
import org.rosi_project.model_management.sync.conflict.customrules.ICustomDeletionRule
import org.rosi_project.model_management.sync.lock.SynchronizationAware
import org.slf4j.LoggerFactory

class MemberCustomDeletionRule extends ICustomDeletionRule {

  private val logger = LoggerFactory.getLogger(classOf[MemberCustomDeletionRule])

  /**
   * get name of this rule
   */
  def getRuleName(): String = "MemberCustomDeletionRule"

  /**
   * return true if no task references comment (the element)
   */
  def matchesRule(element: PlayerSync, senderOwner: String): Boolean = {

    if (element.isInstanceOf[Member]) {
      val elems: Set[SynchronizationAware] = ModelElementLists
        .getDirectElementsFromType("de.tudresden.inf.st.crme.models.modelB.Team")
      elems.foreach{ e =>
        val team: Team = e.asInstanceOf[Team]
        team.getMembers().foreach{ c=>
          if(c.guid == element.asInstanceOf[SynchronizationAware].guid && team.getLeader() == null){
            logger.info("Custom-Deletion-Rule does not allow deletion")
            return false
          }
        }
      }
      logger.info("Custom-Deletion-Rule allows deletion")
      return true
    }

    logger.info("Custom-Deletion-Rule does not allow deletion")
    false
  }
}
