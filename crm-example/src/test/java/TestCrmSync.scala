import de.tudresden.inf.st.crme.models.MetaModels
import de.tudresden.inf.st.crme.models.modelA.PresenceEmployee
import de.tudresden.inf.st.crme.models.modelB.{Member, Team}
import de.tudresden.inf.st.crme.models.modelC.SimpleEmployee
import de.tudresden.inf.st.crme.rsync.compartments.{CrmModelsConstructionCompartment, CrmModelsDestructionCompartment, CrmModelsSyncCompartment}
import de.tudresden.inf.st.crme.rsync.constants.ModelConstants
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.rosi_project.model_management.core.{ModelElementLists, SynchronizationCompartment}
import org.rosi_project.model_management.sync.lock.{ElementLock, ElementLockProvider}
import org.rosi_project.model_management.util.UuidUtil
import org.slf4j.{Logger, LoggerFactory}
import scroll.internal.Compartment

class TestCrmSync {
  val log: Logger = LoggerFactory.getLogger(getClass)

  @Test
  def testSync(){
    new Compartment {
      MetaModels.initCrmMetaModels()

      val lock: ElementLock = ElementLockProvider.provideLockObject()

      val simpleEmployee: SimpleEmployee = new SimpleEmployee("16.07.95", false, "NEWTEAM", "John Doe", "Sample Address 123", lock, UuidUtil.generateUuid(), "owner1")
      val team = new Team("ATeam", null, Set(), lock, UuidUtil.generateUuid(), "owner1")
      val member: Member = new Member("01.01.2001", "Max", "Headquarter", "Muster", "Sample Address 456", team, null, lock,
        UuidUtil.generateUuid(), "owner1")
      new PresenceEmployee("Jane Doe", "05.06.1999", "Sample Address 789", 4000.00f, lock, UuidUtil.generateUuid(), "owner1")

      ElementLockProvider.removeLocks(Set(ModelConstants.MEMBER_MODEL_TYPE, ModelConstants.TEAM_MODEL_TYPE), lock.getId())

      // Assert that all employees have been created as every type

      val presenceEmployees: Set[PresenceEmployee] = ModelElementLists.getElementsFromType("modelA.PresenceEmployee").map(x => x.asInstanceOf[PresenceEmployee])
      assert(presenceEmployees.exists(x => x.getFullName() == "John Doe"))
      assert(presenceEmployees.exists(x => x.getFullName() == "Jane Doe"))
      assert(presenceEmployees.exists(x => x.getFullName() == "Max Muster"))

      val simpleEmployees: Set[SimpleEmployee] = ModelElementLists.getElementsFromType("modelC.SimpleEmployee").map(x => x.asInstanceOf[SimpleEmployee])
      assert(simpleEmployees.exists(x => x.getFullName() == "John Doe"))
      assert(simpleEmployees.exists(x => x.getFullName() == "Jane Doe"))
      assert(simpleEmployees.exists(x => x.getFullName() == "Max Muster"))

      val members: Set[Member] = ModelElementLists.getElementsFromType("modelB.Member").map(x => x.asInstanceOf[Member])
      assert(members.exists(x => x.getFullName() == "John Doe"))
      assert(members.exists(x => x.getFullName() == "Jane Doe"))
      assert(members.exists(x => x.getFullName() == "Max Muster"))


      // ModelElementLists.printAll()
      // log.error("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

      simpleEmployee.setTeam("TEAM TEAM TEAM")
      simpleEmployee.syncFieldsToOtherModels()

      // assert(ModelElementLists.getElementsFromType("modelB.Member").map(x => x.asInstanceOf[Member]).exists(x => x.getMemberInTeam() != null && x.getMemberInTeam().getTeamName() == "TEAM TEAM TEAM"))

      member.setFirstName("Sebastian")
      member.syncFieldsToOtherModels()

      assert(ModelElementLists.getElementsFromType("modelC.SimpleEmployee").map(x => x.asInstanceOf[SimpleEmployee]).exists(x => x.getFullName() == "Sebastian Muster"))

      team.addMember(member)

      assert(ModelElementLists.getElementsFromType("modelC.SimpleEmployee").map(x => x.asInstanceOf[SimpleEmployee]).exists(x => x.getTeam() != null && x.getTeam() == team.getTeamName() && x.getFullName() == "Sebastian Muster"))

      //ModelElementLists.printAll()
      //log.error("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%--")

      team.deleteObjectFromSynchro()
      member.deleteObjectFromSynchro()
      ModelElementLists.printAll()

      assert(ModelElementLists.getElementsFromType("modelB.SimpleEmployee").map(x => x.asInstanceOf[SimpleEmployee]).forall(x => !(x.getFullName() == member.getFullName())))

      // log.error("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%--")
      // assertEquals("Hello", "Hello");
    }
  }
}
