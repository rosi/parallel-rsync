import de.tudresden.inf.st.crme.models.MetaModels
import de.tudresden.inf.st.crme.models.modelB.Team
import de.tudresden.inf.st.crme.models.modelC.SimpleEmployee
import de.tudresden.inf.st.crme.rsync.compartments.{CrmModelsConstructionCompartment, CrmModelsDestructionCompartment, CrmModelsSyncCompartment}
import de.tudresden.inf.st.crme.rsync.constants.ModelConstants
import de.tudresden.inf.st.crme.rules.customrules.{MemberCustomDeletionRule, MemberCustomReferenceCreationRule, MemberCustomReferenceDeletionRule}
import de.tudresden.inf.st.crme.rules.systemrules.{OwnerDataConflictSystemRule, TsDataConflictSystemRule, TsReferenceConflictSystemRule}
import org.junit.jupiter.api.{BeforeAll, BeforeEach, Test}
import org.rosi_project.model_management.{ClientChangeRecord, ConflictResolution, ConflictResolutionResponse}
import org.rosi_project.model_management.core.{ModelElementLists, SynchronizationCompartment}
import org.rosi_project.model_management.sync.conflict.customrules.CustomRuleExecutor
import org.rosi_project.model_management.sync.conflict.systemrules.SystemRuleExecutor
import org.rosi_project.model_management.sync.procedures.data.ClientEntireRecord
import org.rosi_project.model_management.util.{PerformanceCounter, PerformanceInformation}
import org.slf4j.{Logger, LoggerFactory}
import org.testng.annotations.BeforeClass
import scroll.internal.Compartment

import java.util.concurrent.{Callable, Executors}
import scala.collection.mutable.ListBuffer
import scala.sys.error

class CrmConflictBenchmark {
  val log: Logger = LoggerFactory.getLogger(getClass)

  @BeforeEach
  def init(): Unit ={
    MetaModels.initCrmMetaModels()
  }

  def createSimpleElements(count: Int): (ConflictResolutionResponse, Seq[String]) ={
    // Initial creation of elements
    val clientChangeRecords = ListBuffer[ClientChangeRecord]()
    val clientEntireRecords = ListBuffer[ClientEntireRecord]()

    for(i <- 1 to count) {
      clientChangeRecords.append(ClientChangeRecord(ModelConstants.SIMPLE_EMPLOYEE_MODEL_TYPE, s"e${i} t${Thread.currentThread().getId}", 0, 0,
        Map("birthday" -> "01.01.1995", "fullName" -> s"Member ${i}", "address" -> "Dresden"), Map(), Map(), s"e${i} t${Thread.currentThread().getId}", s"testuser${Thread.currentThread().getId}", refonly = false))
      clientEntireRecords.append(ClientEntireRecord(s"e${i} t${Thread.currentThread().getId}", 0, 0, ModelConstants.SIMPLE_EMPLOYEE_MODEL_TYPE))
    }

    val initResponse = ConflictResolution.syncModel("creation", clientChangeRecords, clientEntireRecords, count, s"testuser${Thread.currentThread().getId}")
    val employeeGuids = initResponse.changes.filter(x => x.model == ModelConstants.SIMPLE_EMPLOYEE_MODEL_TYPE).map(x => x.guid)
    //    val teams: Seq[Team] = for(i <- 1 to count)
    //      yield ModelElementLists.getDirectElementsFromType(ModelConstants.TEAM_MODEL_TYPE).find(p => p.asInstanceOf[Team].getTeamName().equals(s"Team ${i}")).get.asInstanceOf[Team]

    (initResponse, employeeGuids)
  }

  def createElements(count: Int, teamCount: Int): (ConflictResolutionResponse, Seq[String], Seq[String]) ={
    // Initial creation of elements
    val clientChangeRecords = ListBuffer[ClientChangeRecord]()
    val clientEntireRecords = ListBuffer[ClientEntireRecord]()

    for(i <- 0 until count) {
      clientChangeRecords.append(ClientChangeRecord(ModelConstants.MEMBER_MODEL_TYPE, s"e${i} t${Thread.currentThread().getId}", 0, 0,
        Map("birthday" -> s"${i}.01.1995", "lastName" -> s"Member ${i}", "address" -> "Dresden"), Map(), Map(), s"e${i} t${Thread.currentThread().getId}", s"testuser${Thread.currentThread().getId}", refonly = false))
      clientEntireRecords.append(ClientEntireRecord(s"e${i} t${Thread.currentThread().getId}", 0, 0, ModelConstants.MEMBER_MODEL_TYPE))
    }
    for(i <- 0 until teamCount){
      clientChangeRecords.append(ClientChangeRecord(ModelConstants.TEAM_MODEL_TYPE, s"t${i} t${Thread.currentThread().getId}", 0, 0,
        Map("teamName" -> s"Team ${i}"), Map(), Map(), s"t${i} t${Thread.currentThread().getId}", s"testuser${Thread.currentThread().getId}", refonly = false))
      clientEntireRecords.append(ClientEntireRecord(s"t${i} t${Thread.currentThread().getId}", 0, 0, ModelConstants.TEAM_MODEL_TYPE))
    }

    val initResponse = ConflictResolution.syncModel("creation", clientChangeRecords, clientEntireRecords, count, s"testuser${Thread.currentThread().getId}")
    val employeeGuids = initResponse.changes.filter(x => x.model == ModelConstants.MEMBER_MODEL_TYPE).map(x => x.guid)
    val teamGuids = initResponse.changes.filter(x => x.model == ModelConstants.TEAM_MODEL_TYPE).map(x => x.guid)

    if(employeeGuids.length != count)
      error("Employee count does not match")
    if(teamGuids.length != teamCount)
      error("Team count does not match")

//    val teams: Seq[Team] = for(i <- 1 to count)
//      yield ModelElementLists.getDirectElementsFromType(ModelConstants.TEAM_MODEL_TYPE).find(p => p.asInstanceOf[Team].getTeamName().equals(s"Team ${i}")).get.asInstanceOf[Team]

    (initResponse, employeeGuids, teamGuids)
  }

  @Test
  def testSimpleCreation(): Unit = {
    MetaModels.reset()
    createElements(1, 1)
    ModelElementLists.printAll()
  }

  @Test
  def testCreation(): Unit ={
    for(m <- 1 to 2) {
      val times = ListBuffer[PerformanceInformation]()
      for (count <- 100 to 500 by 100) {
        for (n <- 0 until 5) {
          println(s"${n} ${count}")
          MetaModels.reset()
          val (response, _, _) = createElements(count, 0)
          times.append(response.performance)
        }
      }
      PerformanceCounter.writeRecords(times, "../../evaluation/bench_creation.csv")
    }
  }

  def oneDeletionIteration(count: Int, threadCount: Int): PerformanceInformation = {
    val (creationResult, employeeGuids, teamGuids) = createElements(count, 0)

    val clientChangeRecords = ListBuffer[ClientChangeRecord]()
    val clientEntireRecords = ListBuffer[ClientEntireRecord]()

    val countConflicts = Math.ceil(count / 10.0).toInt
    for (i <- 0 until count) {
      val ts = if (i % countConflicts == 0) 3 else 2
      clientChangeRecords.append(ClientChangeRecord(ModelConstants.MEMBER_MODEL_TYPE, employeeGuids(i), 1, -ts,
        Map("birthday" -> "01.01.1995", "fullName" -> s"Member ${i}", "address" -> "Dresden", "salary" -> "0.0"), Map(), Map(), s"e${i} t${Thread.currentThread().getId}", s"${Thread.currentThread().getId}", refonly = false))
      clientEntireRecords.append(ClientEntireRecord(employeeGuids(i), -ts, 1, ModelConstants.MEMBER_MODEL_TYPE))
    }

    val result = ConflictResolution.syncModel("deletion", clientChangeRecords, clientEntireRecords, count * 3, s"testuser${Thread.currentThread().getId}", threadCount)

    result.performance
  }

  def oneModificationIteration(count: Int, threadCount: Int): PerformanceInformation = {
    val clientChangeRecords = ListBuffer[ClientChangeRecord]()
    val clientEntireRecords = ListBuffer[ClientEntireRecord]()
    val countConflicts = Math.ceil(count / 10.0).toInt

    val (creationResult, employeeGuids, teamGuids) = createElements(count, 0)

    // Change elements: Some with lower ts than already registered
    for (i <- 0 until count) {
      val ts = if (i % countConflicts == 0) 2 else 1
      clientChangeRecords.append(ClientChangeRecord(ModelConstants.MEMBER_MODEL_TYPE, employeeGuids(i), 1, ts,
        Map("birthday" -> s"0${i}.01.1995", "fullName" -> s"Member ${i}", "address" -> "Dresden", "salary" -> "0"), Map(), Map(), s"e${i} t${Thread.currentThread().getId}", s"${Thread.currentThread().getId}", refonly = false))
      clientEntireRecords.append(ClientEntireRecord(employeeGuids(i), ts, 1, ModelConstants.MEMBER_MODEL_TYPE))
    }
    val modificationResult = ConflictResolution.syncModel("modification", clientChangeRecords, clientEntireRecords, count * 3, s"testuser${Thread.currentThread().getId}", threadCount)

    modificationResult.performance
  }

  def oneReferenceDeletionIteration(count: Int, threadCount: Int): PerformanceInformation = {
    val countConflicts = Math.ceil(count / 10.0).toInt

    val clientChangeRecords = ListBuffer[ClientChangeRecord]()
    val clientEntireRecords = ListBuffer[ClientEntireRecord]()
   // val clientChangeRecords2 = ListBuffer[ClientChangeRecord]()
   // val clientEntireRecords2 = ListBuffer[ClientEntireRecord]()

    val (creationResult, employeeGuids, teams) = createElements(count, count)

    for(i <- 0 until count) {
      //val rv = if (i % countConflicts == 0) 3 else 2
      clientChangeRecords.append(ClientChangeRecord(ModelConstants.TEAM_MODEL_TYPE, teams(i), 1, 1,
        Map(), Map(), Map("members" -> Set(employeeGuids(i))), s"Team0", s"${Thread.currentThread().getId}", refonly = false))
  //    clientEntireRecords.append(ClientEntireRecord(employeeGuids(i), 2, 1, ModelConstants.EMPLOYEE_MODEL_TYPE))
      clientEntireRecords.append(ClientEntireRecord(teams(i), 1, 1, ModelConstants.TEAM_MODEL_TYPE))

      if (i % countConflicts == 0) {
        clientChangeRecords.append(ClientChangeRecord(ModelConstants.TEAM_MODEL_TYPE, teams(i), 2, 1,
          Map(), Map(), Map("members" -> Set(employeeGuids(i))), "Team0", s"${Thread.currentThread().getId}", refonly = false))
        //clientEntireRecords.append(ClientEntireRecord(employeeGuids(i), 3, 1, ModelConstants.EMPLOYEE_MODEL_TYPE))
        //clientEntireRecords.append(ClientEntireRecord(teamGuids(i), 3, 1, ModelConstants.TEAM_MODEL_TYPE))
      }
    }

    val result1 = ConflictResolution.syncModel("referencedeletion", clientChangeRecords, clientEntireRecords, count * 3, s"testuser${Thread.currentThread().getId}", threadCount)
    val r1performance = result1.performance

   // val result2 = ConflictResolution.syncModel("referencedeletion", clientChangeRecords2, clientEntireRecords2)
  //  val r2performance = result2.performance

    r1performance
   //PerformanceInformation(r2performance.thread, r2performance.lockFailures + r1performance.lockFailures, r2performance.name, r1performance.time + r2performance.time, count, r1performance.threads)
  }

  @Test
  def testModification(): Unit ={
    for(m <- 1 to 2) {
      val times = ListBuffer[PerformanceInformation]()
      for (count <- 30 to 300 by 30) {
        println(s"${count}")
        for (n <- 1 to 8) {
          MetaModels.reset()

          times.append(oneModificationIteration(count, 1))
        }
      }
      PerformanceCounter.writeRecords(times, "../../evaluation/bench_modification.csv")
    }
  }

  @Test
    def testModificationParallel() {
    val perflist = ListBuffer[PerformanceInformation]()
    val executor = Executors.newFixedThreadPool(4)

    for (threads <- 1 to 4) {
      for (count <- 30 to 300 by 30) {
        val m = if(threads == 1) 6 else 3
        for (n <- 1 to m) {
          println(s"THREADS: ${threads} COUNT: ${count} N: ${n}")

          MetaModels.reset()

          val run: Callable[PerformanceInformation] = () => oneModificationIteration(count, threads)
          val futures = for (i <- 0 until threads) yield executor.submit(run)

          futures.foreach { f =>
            val perf = f.get()
            perflist.append(perf)
          }
        }
      }
    }

    PerformanceCounter.writeRecords(perflist, "../../evaluation/bench_modification_parallel.csv")
  }

  @Test
  def testDeletion(): Unit = {
    val times = ListBuffer[PerformanceInformation]()

    for(m <- 1 to 2) {
      for (count <- 30 to 300 by 30) {
        for (n <- 1 to 5) {
          println(s"COUNT: ${count} N: ${n}")

          MetaModels.reset()
          val result = oneDeletionIteration(count, 1)
          times.append(result)
        }
      }

    PerformanceCounter.writeRecords(times, "../../evaluation/bench_deletion.csv")
    }
  }

  @Test
  def testDeletionParallel(): Unit ={
    val perflist = ListBuffer[PerformanceInformation]()
    val executor = Executors.newFixedThreadPool(4)

    for (threads <- 1 to 4) {
      for (count <- 30 to 300 by 30) {
        for (n <- 1 to 4) {
          println(s"THREADS: ${threads} COUNT: ${count} N: ${n}")

          MetaModels.reset()

          val run: Callable[PerformanceInformation] = () => oneDeletionIteration(count, threads)
          val futures = for (i <- 0 until threads) yield executor.submit(run)

          futures.foreach { f =>
            val perf = f.get()
            perflist.append(perf)
          }
        }
      }
    }

    PerformanceCounter.writeRecords(perflist, "../../evaluation/bench_deletion_parallel.csv")
  }

  @Test
  def testReferenceDeletion(): Unit = {
    for(m <- 1 to 2) {
      val times = ListBuffer[PerformanceInformation]()

      // Delete some elements: Some twice
      for (count <- 30 to 300 by 30) {
        for (n <- 1 to 6) {
          println(s"COUNT: ${count} N: ${n}")

          MetaModels.reset()

          val performanceInformation = oneReferenceDeletionIteration(count, 1)
          times.append(performanceInformation)
        }
      }

      PerformanceCounter.writeRecords(times, "../../evaluation/bench_reference_deletion.csv")
    }
  }

  @Test
  def testParallelReferenceDeletion(): Unit = {
    val executor = Executors.newFixedThreadPool(4)

    for(m <- 1 to 2) {
      val times = ListBuffer[PerformanceInformation]()

      // Delete some elements: Some twice
      for (threads <- 1 to 4) {
        for (count <- 30 to 300 by 30) {
          val iter = if(threads == 1) 8 else 5
          for (n <- 1 to iter) {
            println(s"THREADS: ${threads} COUNT: ${count} N: ${n}")

            MetaModels.reset()

            val run: Callable[PerformanceInformation] = () => oneReferenceDeletionIteration(count, threads)
            val futures = for (i <- 0 until threads) yield executor.submit(run)

            futures.foreach { f =>
              val perf = f.get()
              times.append(perf)
            }
          }
        }
      }

      PerformanceCounter.writeRecords(times, "../../evaluation/bench_reference_deletion_parallel.csv")
    }
  }
}
