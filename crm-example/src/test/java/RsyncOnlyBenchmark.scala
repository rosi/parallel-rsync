import de.tudresden.inf.st.crme.models.MetaModels
import de.tudresden.inf.st.crme.models.modelA.Employee
import de.tudresden.inf.st.crme.models.modelB.{Member, Team}
import de.tudresden.inf.st.crme.models.modelC.SimpleEmployee
import de.tudresden.inf.st.crme.rsync.constants.ModelConstants
import org.junit.jupiter.api.{BeforeAll, BeforeEach, Test}
import org.rosi_project.model_management.core.ModelElementLists
import org.rosi_project.model_management.util.{PerformanceCounter, RsyncOnlyPerformanceInformation}

import scala.collection.mutable.ListBuffer


class RsyncOnlyBenchmark {
  @BeforeEach
  def init(): Unit ={
    MetaModels.initCrmMetaModels()
  }

  def createElementsRsyncOnly(count: Int): Seq[Member] ={
    MetaModels.reset()
    val t = new Team("Team 0", null, Set(), null, "t0", "user1")
    val employees = for(i <- 1 to count) yield new Member("01.01.1995", "Tim", "Dresden", "K", "Dresden", t, t, null, s"e${i}", "user1")

    employees
  }

  @Test
  def modificationRsyncOnly: Unit = {
    for(j <- 1 to 2) {
      val times = new ListBuffer[RsyncOnlyPerformanceInformation]()

      for (count <- 30 to 300 by 30) {
        val countConflicts = Math.ceil(count / 10.0).toInt

        println(s"${count}")
        val employees = createElementsRsyncOnly(count)

        for (n <- 1 to 10) {
          val start = System.nanoTime()
          var i = 0
          employees.foreach { e =>
            e.setBirthday(s"0${n}.01.1995")
            e.syncFieldsToOtherModels()
            if(i % countConflicts == 0){
              e.setBirthday(s"test")
              e.syncFieldsToOtherModels()
            }
            i += 1
          }
          val duration = System.nanoTime() - start
          times.append(RsyncOnlyPerformanceInformation("modification", duration, count * 3))
        }
      }

      PerformanceCounter.writeRsyncRecords(times, "../../evaluation/bench_modification_rsync.csv")
    }
  }

  @Test
  def deletionRsyncOnly: Unit = {
    val times = new ListBuffer[RsyncOnlyPerformanceInformation]()

    for(j <- 1 to 2) {
      for (count <- 30 to 300 by 30) {
        println(s"${count}")
        val employees = createElementsRsyncOnly(count)

        for (n <- 1 to 8) {
          val start = System.nanoTime()
          employees.foreach { e =>
            e.deleteObjectFromSynchro()
          }
          val duration = System.nanoTime() - start
          times.append(RsyncOnlyPerformanceInformation("modification", duration, count * 3))
        }
      }

      PerformanceCounter.writeRsyncRecords(times, "../../evaluation/bench_deletion_rsync.csv")
    }
  }

  @Test
  def referenceDeletionRsyncOnly: Unit = {
    val times = new ListBuffer[RsyncOnlyPerformanceInformation]()

    for (j <- 1 to 2) {
      for (count <- 30 to 300 by 30) {
        println(s"${count}")
        val employees = createElementsRsyncOnly(count)

        for (n <- 1 to 8) {
          val start = System.nanoTime()
          employees.foreach { e =>
            e.setMemberInTeam(null)
            e.syncFieldsToOtherModels()
          }
          val duration = System.nanoTime() - start
          times.append(RsyncOnlyPerformanceInformation("referencedeletion", duration, count * 3))
        }
      }

      PerformanceCounter.writeRsyncRecords(times, "../../evaluation/bench_reference_deletion_rsync.csv")
    }
  }
}
